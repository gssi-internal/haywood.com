﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Transactions
/// </summary>
public class Transactions : IComparable
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Type { get; set; }
    public string Amount { get; set; }
    public string Information { get; set; }
    public string Date { get; set; }
    public string Category { get; set; }
    public string Logo { get; set; }
 

    public Transactions()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Transactions(Guid id, string title, string type, string amount, string information, string date, string category, string logo)
    {
        this.Id = id;
        this.Title = title;
        this.Type = type;
        this.Amount = amount;
        this.Information = information;
        this.Date = date;
        this.Category = category;
        this.Logo = logo;
      
    }

    public int CompareTo(object obj)
    {
        Transactions r = (Transactions)obj;
        return Title.CompareTo(r.Title);
    }
}