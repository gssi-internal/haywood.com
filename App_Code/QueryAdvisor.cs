﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

/// <summary>
/// Summary description for QueryAdvisor
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class QueryAdvisor : System.Web.Services.WebService
{

    public QueryAdvisor()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }


    [WebMethod(EnableSession = true)]
    public List<string> GetUserIACode(string term)
    {
        List<string> listProductCodes = GetSecurityAdvisors();

        term = term.Trim().ToLower();

        //if (listProductCodes != null && !string.IsNullOrEmpty(term))
        if (listProductCodes != null)
        {
            //listProductCodes = listProductCodes.Where(x => x.ToLower().Contains(term.ToLower().Trim())).ToList();
            List<string> listStartWith = new List<string>();
            List<string> listNotStartWith = new List<string>();
            List<string> listContaints = new List<string>();

            listStartWith = listProductCodes.Where(o => o.ToLower().StartsWith(term)).ToList();
            listNotStartWith = listProductCodes.Where(o => o.ToLower().StartsWith(term) == false).ToList();

            listContaints = listNotStartWith.Where(o => o.ToLower().Contains(term)).ToList();

            if (listStartWith != null && listStartWith.Count > 0 && listContaints != null && listContaints.Count > 0)
            {
                listProductCodes = listStartWith.Concat(listContaints).ToList();
            }
            else if ((listStartWith != null && listStartWith.Count > 0) && (listContaints == null || listContaints.Count <= 0))
            {
                listProductCodes = listStartWith;
            }
            else if ((listStartWith == null || listStartWith.Count <= 0) && (listContaints != null && listContaints.Count > 0))
            {
                listProductCodes = listContaints;
            }
            else
            {
                listProductCodes = listProductCodes.Where(x => x.ToLower().Contains(term)).ToList();
            }
        }

        listProductCodes.Add("-----------------");
        listProductCodes.Add("Advisor Not Found");

        return listProductCodes;
    }

    protected List<string> GetSecurityAdvisors()
    {
        List<string> listCollection = new List<string>();


        try
        {
            IQueryable<DynamicContent> myCollection = null;
            // Set the provider name for the DynamicModuleManager here. All available providers are listed in
            // Administration -> Settings -> Advanced -> DynamicModules -> Providers
            var providerName = String.Empty;

            // Set a transaction name
            var transactionName = "AdvisorTransactionName";

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
            Type advisorType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.SecurityAdvisor.Securityadvisor");


            // This is how we get the collection of Advisor items
            myCollection = dynamicModuleManager.GetDataItems(advisorType).Where(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live && x.Visible == true
                            && x.GetValue<bool>("IsActive") == true);
            foreach (DynamicContent item in myCollection)
            {
                if (item.GetValue<Lstring>("Title") != null)
                    listCollection.Add(item.GetValue<Lstring>("Title").ToString());
            }

        }
        catch (Exception ex)
        {
            WriteLog("SecurityForm", "Get security advisor information from Sitefinity module" + "\r\n" + ex.Message + "\r\n" + ex.StackTrace);
        }

        return listCollection;
    }

    protected void WriteLog(string fileName, string message)
    {

        var filePath = System.AppDomain.CurrentDomain.BaseDirectory + @"App_data\log\";
        fileName = fileName + "-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
        string fullPath = filePath + fileName;
        if (File.Exists(fullPath))
        {
            //File.Delete(fullPath);
        }
        else
        {
            Directory.CreateDirectory(filePath);
        }

        FileStream filewriter = new FileStream(fullPath, FileMode.Append, FileAccess.Write);
        using (StreamWriter streamWriter = new StreamWriter(filewriter))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(message);
            streamWriter.WriteLine("{0} {1} \r\n {2}", DateTime.Now.ToLongTimeString(),
            DateTime.Now.ToLongDateString(), sb.ToString());

            streamWriter.Write("\r\n");
            streamWriter.Write("========================================================");
            streamWriter.Write("\r\n");
            streamWriter.Flush();
            streamWriter.Close();
        }
        filewriter.Close();
    }

}
