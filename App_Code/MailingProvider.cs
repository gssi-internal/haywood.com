﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Net.Configuration;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Services.Notifications.Configuration;
using System.Globalization;
/// <summary>
/// Summary description for MailingProvider
/// </summary>
public class MailingProvider
{
    public string FromName { get; set; }
    public string FromAddr { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    public bool IsBodyHtml { get; set; }
    /// <summary>
    /// Email Addresses(To) split with , or ;
    /// </summary>
    public string ToAddr { get; set; }
    /// <summary>
    /// Email Addresses(CC) split with , or ;
    /// </summary>
    public string CCAddr { get; set; }

    public string[] Attachments { get; set; }
    //public List<CustomUploadedFileInfo> UploadedFiles { get; set; }

    public string AttachedFilePath { get; set; }
    public string AttachedFileName { get; set; }

    public string AttachedFilePath2 { get; set; }
    public string AttachedFileName2 { get; set; }

    public MailingProvider()
    {
        IsBodyHtml = true;
    }

    public MailingProvider(string FromName, string FromAddr, string ToAddr, string CCAddr, string Subject, string Body, bool IsBodyHtml)
    {
        this.FromName = FromName;
        this.FromAddr = FromAddr;
        this.ToAddr = ToAddr;
        this.CCAddr = CCAddr;
        this.Subject = Subject;
        this.Body = Body;
        this.IsBodyHtml = IsBodyHtml;
    }
    public MailingProvider(string ToAddr, string Subject, string Body)
        : this(string.Empty, string.Empty, ToAddr, string.Empty, Subject, Body, true)
    {
    }

    public string ErrorMessage
    {
        get;
        set;
    }
    
    
    private bool IsValidEmail(string email)
    {
        //regular expression pattern for valid email
        //addresses, allows for the following domails:
        //com, edu, info, gov, int, mil, net, org, biz, name, museum, coop, aero, pro, tv
        var pattern = @"^\s*([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]+)*)@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]+)*)((\.[a-zA-Z0-9_-]{2,3}){1,2})\s*$";

        var check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
        var valid = !string.IsNullOrEmpty(email) && check.IsMatch(email);

        return valid;
    }
    

    public void StartSending()
    {
        SendingMail();
    }
    public void StartAsyncSending()
    {
        lock (this)
        {
            Thread mailThread = new Thread(new ThreadStart(SendingMail));
            mailThread.Start();
        }
    }

    private void SendingMail()
    {
        try
        {
            var mailServer = new SmtpClient();
            var profile = SystemManager.GetNotificationService().GetDefaultSenderProfile(null, SmtpSenderProfileProxy.SmtpProfileType);
            if (profile != null)
            {
                var host = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Host];
                var port = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Port];
                var username = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Username];
                var password = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Password];
                var useSSL = profile.CustomProperties[SmtpSenderProfileProxy.Keys.UseSsl];
                var defaultSenderEmailAddress = profile.CustomProperties[SmtpSenderProfileProxy.Keys.DefaultSenderEmailAddress];
                var defaultSenderName = profile.CustomProperties[SmtpSenderProfileProxy.Keys.DefaultSenderName];


                mailServer.Host = host;
                int portNumber = 25;
                if (int.TryParse(port, out portNumber))
                {
                    mailServer.Port = portNumber;
                }

                bool enableSSL = false;
                if (bool.TryParse(useSSL, out enableSSL))
                {
                    mailServer.EnableSsl = enableSSL;
                }

                //mailServer.Timeout = smtpSettings.Timeout;

                if (!string.IsNullOrEmpty(username))
                {
                    mailServer.UseDefaultCredentials = false;
                    mailServer.Credentials = new NetworkCredential(username, password);
                }


                // init MailMessage
                var mailMsg = new MailMessage();
                this.FromAddr = this.FromAddr.IsNullOrEmpty() ? defaultSenderEmailAddress : this.FromAddr;
                if (this.IsValidEmail(this.FromAddr))
                    mailMsg.From = new MailAddress(this.FromAddr, this.FromAddr.Split('@')[0]);
                if (!string.IsNullOrEmpty(this.ToAddr))
                {
                    var arrToAddr = this.ToAddr.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var emailAddr in arrToAddr.Select(s => s.Trim()).Where(IsValidEmail))
                        mailMsg.To.Add(new MailAddress(emailAddr));
                }
                if (!string.IsNullOrEmpty(this.CCAddr))
                {
                    var arrCCAddr = this.CCAddr.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var emailAddr in arrCCAddr.Select(s => s.Trim()).Where(IsValidEmail))
                        mailMsg.Bcc.Add(new MailAddress(emailAddr));
                }

                if (!String.IsNullOrEmpty(this.AttachedFilePath))
                {
                    Attachment atch = new Attachment(this.AttachedFilePath);
                    atch.Name = this.AttachedFileName;
                    mailMsg.Attachments.Add(atch);
                }

                if (!String.IsNullOrEmpty(this.AttachedFilePath2))
                {
                    Attachment atch2 = new Attachment(this.AttachedFilePath2);
                    atch2.Name = this.AttachedFileName2;
                    mailMsg.Attachments.Add(atch2);
                }

                if (Attachments != null)
                {
                    for (int m = 0; m < Attachments.Length; m++)
                    {
                        mailMsg.Attachments.Add(new Attachment(Attachments[m].ToString()));
                    }
                }

                mailMsg.IsBodyHtml = this.IsBodyHtml;
                mailMsg.Subject = this.Subject;
                mailMsg.Body = this.Body;

                mailServer.Send(mailMsg);
                mailMsg.Dispose();

            }
        }
        catch (Exception ex)
        {
            StringBuilder sb = new StringBuilder(ex.Message);
            if (ex.InnerException != null)
            {
                sb.Append("");
                sb.Append(ex.InnerException.Message);
            }
            ErrorMessage = sb.ToString();

            WriteLog("SendingMailError", ErrorMessage);

        }
    }

    public string SendMail()
    {
        string error = "";
        try
        {
            var mailServer = new SmtpClient();
            var profile = SystemManager.GetNotificationService().GetDefaultSenderProfile(null, SmtpSenderProfileProxy.SmtpProfileType);
            if (profile != null)
            {
                var host = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Host];
                var port = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Port];
                var username = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Username];
                var password = profile.CustomProperties[SmtpSenderProfileProxy.Keys.Password];
                var useSSL = profile.CustomProperties[SmtpSenderProfileProxy.Keys.UseSsl];
                var defaultSenderEmailAddress = profile.CustomProperties[SmtpSenderProfileProxy.Keys.DefaultSenderEmailAddress];
                var defaultSenderName = profile.CustomProperties[SmtpSenderProfileProxy.Keys.DefaultSenderName];


                mailServer.Host = host;
                int portNumber = 25;
                if (int.TryParse(port, out portNumber))
                {
                    mailServer.Port = portNumber;
                }

                bool enableSSL = false;
                if (bool.TryParse(useSSL, out enableSSL))
                {
                    mailServer.EnableSsl = enableSSL;
                }

                //mailServer.Timeout = smtpSettings.Timeout;

                if (!string.IsNullOrEmpty(username))
                {
                    mailServer.UseDefaultCredentials = false;
                    mailServer.Credentials = new NetworkCredential(username, password);
                }


                // init MailMessage
                var mailMsg = new MailMessage();
                this.FromAddr = this.FromAddr.IsNullOrEmpty() ? defaultSenderEmailAddress : this.FromAddr;
                if (this.IsValidEmail(this.FromAddr))
                    mailMsg.From = new MailAddress(this.FromAddr, this.FromAddr.Split('@')[0]);
                if (!string.IsNullOrEmpty(this.ToAddr))
                {
                    var arrToAddr = this.ToAddr.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var emailAddr in arrToAddr.Select(s => s.Trim()).Where(IsValidEmail))
                        mailMsg.To.Add(new MailAddress(emailAddr));
                }
                if (!string.IsNullOrEmpty(this.CCAddr))
                {
                    var arrCCAddr = this.CCAddr.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var emailAddr in arrCCAddr.Select(s => s.Trim()).Where(IsValidEmail))
                        mailMsg.Bcc.Add(new MailAddress(emailAddr));
                }

                if (!String.IsNullOrEmpty(this.AttachedFilePath))
                {
                    Attachment atch = new Attachment(this.AttachedFilePath);
                    atch.Name = this.AttachedFileName;
                    mailMsg.Attachments.Add(atch);
                }

                if (!String.IsNullOrEmpty(this.AttachedFilePath2))
                {
                    Attachment atch2 = new Attachment(this.AttachedFilePath2);
                    atch2.Name = this.AttachedFileName2;
                    mailMsg.Attachments.Add(atch2);
                }

                if (Attachments != null)
                {
                    for (int m = 0; m < Attachments.Length; m++)
                    {
                        mailMsg.Attachments.Add(new Attachment(Attachments[m].ToString()));
                    }
                }

                mailMsg.IsBodyHtml = this.IsBodyHtml;
                mailMsg.Subject = this.Subject;
                mailMsg.Body = this.Body;

                mailServer.Send(mailMsg);
                mailMsg.Dispose();
            }
        }
        catch (Exception ex)
        {
            StringBuilder sb = new StringBuilder(ex.Message);
            if (ex.InnerException != null)
            {
                sb.Append("");
                sb.Append(ex.InnerException.Message);
            }
            ErrorMessage = sb.ToString();
            error = sb.ToString();

            WriteLog("SendingMailError", ErrorMessage + "\r\n" + ex.StackTrace);
        }

        return error;
    }


    public static void WriteLog(string fileName, string message)
    {

        var filePath = System.AppDomain.CurrentDomain.BaseDirectory + @"App_data\log\";
        fileName = fileName + "-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
        string fullPath = filePath + fileName;
        if (File.Exists(fullPath))
        {
            //File.Delete(fullPath);
        }
        else
        {
            Directory.CreateDirectory(filePath);
        }

        FileStream filewriter = new FileStream(fullPath, FileMode.Append, FileAccess.Write);
        using (StreamWriter streamWriter = new StreamWriter(filewriter))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(message);
            streamWriter.WriteLine("{0} {1} \r\n {2}", DateTime.Now.ToLongTimeString(),
            DateTime.Now.ToLongDateString(), sb.ToString());

            streamWriter.Write("\r\n");
            streamWriter.Write("========================================================");
            streamWriter.Write("\r\n");
            streamWriter.Flush();
            streamWriter.Close();
        }
        filewriter.Close();
    }

}