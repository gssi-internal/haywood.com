﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>

using System;
using System.Web;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Modules.News;
using Telerik.Sitefinity.News.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.OpenAccess;


public class ImageHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        StringBuilder html = new StringBuilder();
        context.Response.ContentType = "text/html";
        

        try
        {
            string imageIds = context.Request.QueryString["id"];


            html = html.Append(string.Format("<img alt='image' src='{0}' class='selImg'>",  System.Web.HttpUtility.HtmlEncode (  Telerik.Sitefinity.App.WorkWith().Image(new Guid(imageIds)).Get().MediaUrl).Replace("jpg?", "jpg.tmb?")));
            
            
        }
        catch (Exception exp)
        {
            html = new StringBuilder();
        
        }


        context.Response.Write(html.ToString());
    
    
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}