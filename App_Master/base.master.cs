﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_Master_base : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Telerik.Sitefinity.Services.SystemManager.IsDesignMode == false)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["TempPasswordProtected"] != null &&
                       System.Configuration.ConfigurationManager.AppSettings["TempPasswordProtected"].ToString().ToLower() == "true")
            {
                TempCheckLogin();
            }
        }
            
    }

    protected void TempCheckLogin()
    {
        if (HttpContext.Current.Session["userLogin"] != null && HttpContext.Current.Session["userLogin"].ToString() == "true")
        {

        }
        else
        {
            Response.Redirect("/TempLogin.aspx?returnUrl=" + Request.Url);
        }
    }
}
