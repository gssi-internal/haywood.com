﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_Master_Internalhome : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsDesignMode())
        {
            HttpCookie cookie = Request.Cookies["location"];
            string currentUrl = Request.Url.AbsoluteUri;
            if (currentUrl.ToLower().Contains("/usa"))
            {

                if (cookie == null || cookie.Value == "" || cookie.Value == "ca")
                //     if ((Request.Cookies["location"].Value == "ca") || (Request.Cookies["location"].Value == null) || (Request.Cookies["location"].Value == ""))
                {
                    Response.Redirect("/acceptance");
                }
            }
        }
    }
}
