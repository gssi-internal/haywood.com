﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BroadridgeStock.aspx.cs" Inherits="BroadridgeStock" %>

<%@ Register Src="~/UserControls/GSSI/GetStock.ascx" TagPrefix="uc1" TagName="GetStock" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
  font-family: 'Open Sans', sans-serif;
  font-size: 11px;
  line-height: 1.714;
  color: #404049;
  font-weight: 400;
}
        iframe {width:800px;}
        .broad-stock {
  position: absolute;
  top: 29px;
  right: 0;
  width: 100%;
  text-align: right;
  font-size: 11px;
}
        .stockmarket li {
  position: relative;
  margin-left: 30px;
  display: inline-block;font-family: 'Open Sans', sans-serif;
}
        .stockmarket .stock-item .stock-title {
  color: #333;
  font-weight: 700;
}
.stockmarket .stock-item span {
  text-transform: uppercase;
  display: inline-block;
  margin: 0 3px;
}
    </style>
</head>
<body class="broadridge-stock">
    <form id="form1" runat="server">
   
        <ul class="stockmarket mobile-h">
        <uc1:GetStock runat="server" ID="GetStock" />
    </ul>
    </form>
</body>
</html>
