﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TempLogin.aspx.cs" Inherits="TempLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
        <tr>
            <td>User name:</td>
            <td><asp:TextBox runat="server" ID="txtUsername"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><asp:TextBox runat="server" ID="txtPwd" TextMode="Password"></asp:TextBox></td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button runat="server" ID="btnLogin" OnClick="btnLogin_Click" Text="Login" /> </td>
        </tr>
         <tr>
            <td></td>
            <td><asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label> </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
