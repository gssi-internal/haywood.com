﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testEmail.aspx.cs" Debug="true" Inherits="testEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td style="width: 100px">
                    Mail Server:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txt_server" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Port:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txt_port" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Username:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txt_username" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Password:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txt_password" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    From Email:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txt_from" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    To Email:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txt_to" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    <asp:Button ID="btn_send" runat="server" OnClick="btn_send_Click" Text="Send a test email by input settings" /></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
