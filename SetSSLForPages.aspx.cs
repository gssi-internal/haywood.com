﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Pages;

public partial class SetSSLForPages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    
    protected void Turnssl_On(object sender, EventArgs e)
    {
        var choice = sslchoice.Text;

        PageManager pageManager = PageManager.GetManager();
        pageManager.Provider.SuppressSecurityChecks = true;
        var pages = pageManager.GetPageNodes().ToList();

        if (choice == "All")
        {
            foreach (var page in pages)
            {
                if (page.Page != null)
                {
                    page.Page.RequireSsl = true;
                }

                pageManager.SaveChanges();
            }
            pageManager.SaveChanges();
        }
        else if (choice == "Frontend")
        {
            foreach (var page in pages)
            {
                if (page.Page != null && page.Page.IsBackend == false)
                {
                    page.Page.RequireSsl = true;
                }

                pageManager.SaveChanges();
            }
            pageManager.SaveChanges();
        }
        else
        {
            foreach (var page in pages)
            {
                if (page.Page != null && page.Page.IsBackend == true)
                {
                    page.Page.RequireSsl = true;
                }

                pageManager.SaveChanges();
            }
            pageManager.SaveChanges();
        }
    }

    protected void Turnssl_Off(object sender, EventArgs e)
    {
        var choice = sslchoice.Text;

        PageManager pageManager = PageManager.GetManager();
        pageManager.Provider.SuppressSecurityChecks = true;
        var pages = pageManager.GetPageNodes().ToList();

        if (choice == "All")
        {
            foreach (var page in pages)
            {
                if (page.Page != null)
                {
                    page.Page.RequireSsl = false;
                }

                pageManager.SaveChanges();
            }
            pageManager.SaveChanges();
        }
        else if (choice == "Frontend")
        {
            foreach (var page in pages)
            {
                if (page.Page != null && page.Page.IsBackend == false)
                {
                    page.Page.RequireSsl = false;
                }

                pageManager.SaveChanges();
            }
            pageManager.SaveChanges();
        }
        else
        {
            foreach (var page in pages)
            {
                if (page.Page != null && page.Page.IsBackend == true)
                {
                    page.Page.RequireSsl = false;
                }

                pageManager.SaveChanges();
            }
            pageManager.SaveChanges();
        }
    }
}