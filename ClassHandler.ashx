﻿<%@ WebHandler Language="C#" Class="SitefinityImage" %>

using System;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

public class ClassHandler : IHttpHandler
{
         
   
    public void ProcessRequest (HttpContext context) {

        StringBuilder html = new StringBuilder();
        context.Response.ContentType = "text/html";

        try
        {
            string imageIds = context.Request.QueryString["id"];
         

            string[] guids = imageIds.Split(new char[] { ';' });

            if (showAll == "1")
            {
                html.Append("<ul>");

                for (int i = 0; i < guids.Length; i++)
                {
                    
                }

                html.Append("</ul>");
            }
            else
            {
                if (isThumb == "1")
                    html = html.Append(string.Format("<img alt='' src='{0}' class='selImg'>", Telerik.Sitefinity.App.WorkWith().Image(new Guid(guids[0])).Get().MediaUrl).Replace("jpg?", "jpg.tmb?"));
                else
                    html = html.Append(string.Format("<img alt='' src='{0}'>", Telerik.Sitefinity.App.WorkWith().Image(new Guid(guids[0])).Get().MediaUrl));
            }
        }
        catch
        {
        }
        
        context.Response.Write(html.ToString());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    
}