<%@ Control Language="C#" ClassName="FormMailBrowser" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script runat="server">
	private string _loginName = "";
	private string _loginPassword = "";
	private bool _useSecurity = false;
	private string _inputCssClass = "formmail-input";
	private string _labelCssClass = "formmail-label";
	private string _tableCssClass = "formmail-table";
	private string _containerCssClass = "formmail-container";
	private string _errorCssClass = "formmail-error";
	private string _formSaveDirectory = "~/App_Data/formmail/";
	private string _slashEncoding = "^-";

	private List<FileInfo> xmlfiles = new List<FileInfo>();
	
	public string LoginName { get { return _loginName; } set { _loginName = value; } }
	public string LoginPassword { get { return _loginPassword; } set { _loginPassword = value; } }
	public bool UseSecurity { get { return _useSecurity; } set { _useSecurity = value; } }
	public string InputCssClass { get { return _inputCssClass; } set { _inputCssClass = value; } }
	public string TableCssClass { get { return _tableCssClass; } set { _tableCssClass = value; } }
	public string LabelCssClass { get { return _labelCssClass; } set { _labelCssClass = value; } }
	public string ContainerCssClass { get { return _containerCssClass; } set { _containerCssClass = value; } }
	public string ErrorCssClass { get { return _errorCssClass; } set { _errorCssClass = value; } }
	public string FormSaveDirectory { get { return _formSaveDirectory; } set { _formSaveDirectory = value; } }
	public string SlashEncoding { get { return _slashEncoding; } set { _slashEncoding = value; } }
	
	void Page_Load(object sender, EventArgs e) {
		
	}

	void Page_Init(object sender, EventArgs e) {
		if(UseSecurity && (String.IsNullOrEmpty(LoginName) || String.IsNullOrEmpty(LoginPassword)))
			throw new HttpException("FormMail: You must suppy a username/password to the control if using built-in security. ");
		
		
		if (!Page.IsPostBack) {
			FileList.CssClass = FileData.CssClass = TableCssClass;
			FileListContainer.CssClass = ViewContainer.CssClass = ContainerCssClass;
			
			DirectoryInfo dir = new DirectoryInfo(Server.MapPath(_formSaveDirectory));
			if (dir.Exists) {
				FileInfo[] fs = dir.GetFiles("*.xml");

				FileList.DataSource = fs;
				FileList.DataBind();
			}
			
			if (UseSecurity && Session["FormMail-Authenticated"] == null) {
				BrowserPanels.SetActiveView(LoginPanel);

				LoginContainer.CssClass = ContainerCssClass;

				UserNameLabel.CssClass = LabelCssClass;
				UserName.CssClass = InputCssClass;

				PasswordLabel.CssClass = LabelCssClass;
				Password.CssClass = InputCssClass;

			}
			else
				BrowserPanels.SetActiveView(XMLList);

		}
	}
	void FormLogin(object sender, EventArgs e) {
		if (UserName.Text == LoginName && Password.Text == LoginPassword) {
			Session["FormMail-Authenticated"] = true;
			BrowserPanels.SetActiveView(XMLList);
		}
		else {
			ErrorMessage.Text = "Incorrect username and/or password.";
		}
	}
	protected void FileSelect(object sender, GridViewCommandEventArgs e) {
		if (e.CommandName == "Select" || e.CommandName == "Download") {
			string XMLfile = Server.MapPath(_formSaveDirectory + e.CommandArgument);
			FileInfo file = new FileInfo(XMLfile);
			DataTable dt = new DataTable("formmail");

			if (file.Exists) {
				if(e.CommandName == "Select") {
					BrowserPanels.SetActiveView(XMLData);
					dt.ReadXml(XMLfile);
					FileData.DataSource = dt;
					FileData.DataBind();
					CurrentFile.Value = e.CommandArgument.ToString();
				}
				else {
					Response.ContentType = "text/xml";
					Response.AddHeader("Content-Disposition", "attachment; filename=\"" + e.CommandArgument.ToString() + "\"");
					dt.ReadXml(XMLfile);
					dt.WriteXml(Response.OutputStream, XmlWriteMode.IgnoreSchema, false);
					Response.End();
				}
			}
		}
	}
	void ReturnToList(object sender, EventArgs e) {
		BrowserPanels.SetActiveView(XMLList);
	}
	protected void FileData_PageIndexChanging(object sender, GridViewPageEventArgs e) {
		
		FileData.PageIndex = e.NewPageIndex;
		DataTable dt = new DataTable("formmail");
		dt.ReadXml(Server.MapPath(_formSaveDirectory + CurrentFile.Value));
		FileData.DataSource = dt;
		FileData.DataBind();
	}
</script>

<div class="<%=ErrorCssClass%>"><asp:Label ID="ErrorMessage" runat="server" EnableViewState="false" /></div>

<asp:MultiView ID="BrowserPanels" runat="server">
	<asp:View ID="LoginPanel" runat="server">
		<asp:Panel id="LoginContainer" runat="server">
			
			<span class="formmail-field">
				<asp:label runat="server" ID="UserNameLabel" Text="User name:" AssociatedControlID="UserName" />
				<asp:TextBox runat="server" ID="UserName" />
			</span>
			<span class="formmail-field">
				<asp:label runat="server" ID="PasswordLabel" Text="Password:" AssociatedControlID="Password" />
				<asp:TextBox runat="server" ID="Password" TextMode="Password" />
			</span>
			<asp:Button ID="ButtonLogin" Text="Login" runat="server" OnClick="FormLogin" CssClass="formmail-button" />
		</asp:Panel>
	</asp:View>
	
	<asp:View ID="XMLList" runat="server">
		<asp:Panel ID="FileListContainer" runat="server">
			<asp:GridView runat="server" ID="FileList"
				AutoGenerateColumns="false" OnRowCommand="FileSelect">
				<Columns>
					<asp:TemplateField HeaderText="Form Name">
						<ItemTemplate>
							<%# ((string)(Eval("Name").ToString().Substring(0, (Eval("Name").ToString().Length) - 4))).Replace(_slashEncoding, "/")%>	
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton CommandName="Select" Text="View" 
								CommandArgument='<%# Eval("Name").ToString()%>' runat="server" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
					    <ItemTemplate>
					        <asp:LinkButton CommandName="Download" Text="Download" 
								CommandArgument='<%# Eval("Name").ToString()%>' runat="server" />
					    </ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</asp:Panel>
	</asp:View>
	
	<asp:View ID="XMLData" runat="server">
		<asp:Panel ID="ViewContainer" runat="server">
			<asp:LinkButton ID="NavBack" Text="< Back" runat="server" OnClick="ReturnToList" />
			<asp:HiddenField ID="CurrentFile" runat="server" />
			<asp:GridView runat="server" ID="FileData" AutoGenerateColumns="true" 
				PageSize="25" AllowPaging="true" OnPageIndexChanging="FileData_PageIndexChanging">
			</asp:GridView>
		</asp:Panel>	
	</asp:View>
</asp:MultiView>