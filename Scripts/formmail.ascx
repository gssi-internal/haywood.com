<%@ Control Language="C#" ClassName="FormMail" Debug="true" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script runat="server">
/// <summary>
/// FormMail.NET
/// Version 1.2.1
/// </summary>
/// <remarks>
/// CHANGELOG
/// 2009-09-02	1.2.1	Fixed replacing unused variables in classic templates
/// 2009-08-27	1.2		Added the SendingEmail event, allowing further configuration before sending
/// 2009-06-02	1.1.1	Added LinkButton support
/// 2009-05-05	1.1		Changed filename encoding, added debugging support
/// 2009-03-12  1.0.2   Fixed a bug not allowing inline validators.
/// </remarks>
    
    private string _templatefile = "";
	private string _subject = "";
	private string _recipients = "";
	private string _redirect = "";
	private string _fromaddress = "";
	private int _maxfilelength = 0;
	private string _templateID = "";
	private string _submitButtonID = "";
	private string _embedCSSfile = "";
	private string _emailOutput;
	private bool _saveXML = true;
	private string _formSaveDirectory = "~/App_Data/formmail/";
	private string _XMLfilename = "";
	private string _errorCssClass = "formmail-error";
	private bool _removeLeftoverTemplateItems = true; // Only applies when classic templates are used.
	private string _slashEncoding = "^-";
	private Nullable<bool> _debugMode = null;
	private bool debugMode {
		get {
			if (_debugMode == null) {
				if (!String.IsNullOrEmpty(Request.QueryString["debugformmail"]) &&
					Request.QueryString["debugformmail"].ToLower() == "true")
					_debugMode = true;
				else
					_debugMode = false;
			}
			return (_debugMode == true);
		}
	}
	
	private List<Control> templateFormControls = new List<Control>();
	private List<Control> controlValues = new List<Control>();
	private List<FileUpload> attachedFiles = new List<FileUpload>();
	private Dictionary<string, string> dbValues = new Dictionary<string, string>();
	private List<string> errorMessages = new List<string>();
	private List<string> debugMessages = new List<string>();
		
	public string TemplateFile { get { return _templatefile; } set { _templatefile = value; } }
	public string EmbedCSSFile { get { return _embedCSSfile; } set { _embedCSSfile = value; } }
	public string TemplateID { get { return _templateID; } set { _templateID = value; } }
	public string Subject { get { return _subject; } set { _subject = value; } }
	public string FromAddress { get { return _fromaddress; } set { _fromaddress = value; } }
	public string Recipients { get { return _recipients; } set { _recipients = value; } }
	public string Redirect { get { return _redirect; } set { _redirect = value; } }
	public string SubmitButtonID { get { return _submitButtonID; } set { _submitButtonID = value; } }
	public string EmailOutput { get { return _emailOutput; } }
	public bool SaveXML { get { return _saveXML; } set { _saveXML = value; } }
	public string FormSaveDirectory { get { return _formSaveDirectory; } set { _formSaveDirectory = value; } }
	public string ErrorCssClass { get { return _errorCssClass; } set { _errorCssClass = value; } }
	public bool RemoveLeftoverTemplateItems { get { return _removeLeftoverTemplateItems; } set { _removeLeftoverTemplateItems = value; } }
	public string SlashEncoding { get { return _slashEncoding; } set { _slashEncoding = value; } }
		
	public string XMLFilename { 
		get {
			if (_XMLfilename == "") {
				string filepath = Request.Path.Replace("/", _slashEncoding);
				int pathlength = 220;
				if (filepath.Length < 220) pathlength = filepath.Length;
				
				_XMLfilename = filepath.Substring(0,pathlength) + ".xml";
			}
			return _XMLfilename; 
		} 
		set { _XMLfilename = value; } 
	}
	public event EventHandler Submit;
	public event EventHandler SendingEmail;
	
	// Maximum size of attachments in Kilobytes
	public int MaxFileLength { get { return _maxfilelength; } set { _maxfilelength = value; } }


	private void SaveFormToXML() {
		try {
			DirectoryInfo dir = new DirectoryInfo(Server.MapPath(_formSaveDirectory));
			if (!dir.Exists) {
				Directory.CreateDirectory(Server.MapPath(_formSaveDirectory));
			}
			string XMLfile = Server.MapPath(_formSaveDirectory + XMLFilename);
			FileInfo file = new FileInfo(XMLfile);
			DataTable dt = new DataTable("formmail");
			

			if (file.Exists) {
				dt.ReadXml(XMLfile);
				foreach (KeyValuePair<string, string> kvp in dbValues) {
					if (!dt.Columns.Contains(kvp.Key))
						dt.Columns.Add(kvp.Key, typeof(string));
				}
			}
			else {
				foreach (KeyValuePair<string, string> kvp in dbValues) {
					dt.Columns.Add(kvp.Key, typeof(string));
				}			
			}
			DataRow dr = dt.NewRow();
			foreach (KeyValuePair<string, string> kvp in dbValues) {
				dr[kvp.Key] = kvp.Value;
			}
			dt.Rows.Add(dr);
			dt.WriteXml(XMLfile, XmlWriteMode.WriteSchema, false);
			if (debugMode) debugMessages.Add("Form Saved to XML.");
		}
		catch(Exception ex) {
			errorMessages.Add("There was a problem saving the form data.  Please contact " + FromAddress + ".");
			if (debugMode) debugMessages.Add("Form Saving Error : " + ex.Message);
		}		
	}
	private void SendEmail() {
		
		if (SendingEmail != null)
			SendingEmail(this, EventArgs.Empty);		
		
		MailMessage mm = new MailMessage(_fromaddress, _recipients);
		//mm.From = _fromaddress;
		//mm.To.Add(_recipients);
	
		string templateContent = "";
		if (_templateID == "") {
			FileInfo template = new FileInfo(Server.MapPath(_templatefile));
			StreamReader sr = template.OpenText();
			templateContent = sr.ReadToEnd();
			sr.Close();

			foreach (string key in Request.Form) {
				if (templateContent.IndexOf("**" + key.ToUpper() + "**") > 0) {
					templateContent = templateContent.Replace("**" + key.ToUpper() + "**", Request.Form[key]);
					dbValues.Add(key.ToUpper(), Request.Form[key]);
				}
			}
			foreach (string key in Request.Files) {
				if (templateContent.IndexOf("**" + key.ToUpper() + "**") > 0) {
					HttpPostedFile file = Request.Files[key];
					if (file != null) {
						if (file.FileName.IndexOf(".") > 0)
						{
							mm.Attachments.Add(new Attachment(file.InputStream, file.FileName));
							templateContent = templateContent.Replace("**" + key.ToUpper() + "**", file.FileName + " (attached)");
							dbValues.Add(key.ToUpper(), file.FileName);
						}
					}
				}
			}
			if (_removeLeftoverTemplateItems) {
				templateContent = Regex.Replace(templateContent, @"\*\*.*\*\*", "");
			}
		}
		else {
			Control container = new Control();
			if (!String.IsNullOrEmpty(_templateID)) {
				container = this.Parent.FindControl(_templateID);
				if (container == null) {
					container = FindControlRecursively(this.Page, _templateID);
					if(container == null) {
						throw new HttpException("FormMail: Cannot find control '" + _templateID + "'.");
					}
				}
				if (container != null) {
					if (container.Controls.Count > 0) {
						RecurseControls(container.Controls);
					}
					container.EnableViewState = false;
					templateContent = GetEmailBodyFromControl(container);

					foreach (string key in Request.Files) {
						HttpPostedFile file = Request.Files[key];
						if (file != null) {
							if (file.FileName.IndexOf(".")>0)
							{
								mm.Attachments.Add(new Attachment(file.InputStream, file.FileName));
							}
						}
					}
				}
			}
		}

		if (_saveXML) SaveFormToXML();
		
		mm.Subject = _subject;
		mm.Body = templateContent;
		_emailOutput = templateContent;
		mm.IsBodyHtml = true;
		SmtpClient smtp = new SmtpClient();
		if (smtp.Host == null) {
			string server = Request.ServerVariables["SERVER_NAME"];
			if ((server.IndexOf("gssiwebs.com") > -1) || (server.IndexOf("localhost") > -1)) {
				smtp.Host = "gsdc";
			}
			else {
				smtp.Host = "localhost";
			}
		}

		try {
			smtp.Send(mm);
			if(debugMode) debugMessages.Add("Message successfully sent.");
		}
		catch (Exception ex) {
			errorMessages.Add("There was a problem sending your email.  Please contact " + FromAddress + ".");
			if (debugMode) debugMessages.Add("Emailing Error : " + ex.Message);
		}
		
		if (Submit != null)
			Submit(this, EventArgs.Empty);

		if (_redirect != "") {
			if (!debugMode)
				Response.Redirect(_redirect);
			else
				debugMessages.Add("Debug mode prevented redirection to: " + _redirect);
		}
	}


	private void RecurseControls(ControlCollection controls) {
		
		foreach (Control c in controls) {
			if ((c.Visible) && (c is TextBox 
				|| c is ListControl 
				|| c is FileUpload 
				|| c is CheckBox
				|| c is Button
				|| c is LinkButton
				|| c is ImageButton
				|| c is HtmlInputCheckBox
				|| c is HtmlInputFile
				|| c is HtmlInputText
				|| c is HtmlTextArea
				|| c is HtmlInputRadioButton
				|| c is HtmlSelect
				|| c is HtmlInputButton
                || c is BaseValidator
				)) 
				templateFormControls.Add(c);
			if (c.Controls.Count > 0) {
				RecurseControls(c.Controls);
			}
		}
	}
	private void AddValue(Control c, string value) {
		if (c.NamingContainer.GetType() != typeof(CheckBoxList)) {
			Control ctrl = new LiteralControl(value);
			c.Parent.Controls.AddAt(c.Parent.Controls.IndexOf(c), ctrl);
			controlValues.Add(ctrl);
			c.Visible = false;
			string ctrlID;
			if (!String.IsNullOrEmpty(c.ID))
				ctrlID = c.ID;
			else
				ctrlID = c.ClientID;

			dbValues.Add(ctrlID, value);
		}
	}
	
	// Iterates through the controls that are form-related and puts in values
	private void SwapValues() {
		string value;
		foreach (Control c in templateFormControls) {
		
			if (c is TextBox) {
				AddValue(c, ((TextBox)c).Text);
			}
			else if (c is ListControl) {
				ListControl list = (ListControl)c;
				ArrayList values = new ArrayList();
				for (int i = 0; i < list.Items.Count; i++) {
					if (list.Items[i].Selected) {
						values.Add(list.Items[i].Value);
					}
				}
				
				AddValue(c, String.Join(", ", (string[])values.ToArray(typeof(string))));
			}
			else if (c is HtmlSelect) {
				HtmlSelect list = (HtmlSelect)c;
				ArrayList values = new ArrayList();
				for (int i = 0; i < list.Items.Count; i++) {
					if (list.Items[i].Selected) {
						values.Add(list.Items[i].Value);
					}
				}

				AddValue(c, String.Join(", ", (string[])values.ToArray(typeof(string))));
			}
			else if (c is FileUpload) {
				FileUpload fup = (FileUpload)c;
				if (fup.HasFile) {
					AddValue(c, fup.FileName + " (attached)");
					// attachedFiles.Add(fup);
				}
				else {
					AddValue(c, "(none attached)");
				}
			}
			else if (c is HtmlInputFile) {
				HtmlInputFile fup = (HtmlInputFile)c;
				if (fup.PostedFile.FileName != "") {
					AddValue(c, fup.PostedFile.FileName + " (attached)");
				}
			}
			else if (c is HtmlInputCheckBox) {
				HtmlInputCheckBox chk = (HtmlInputCheckBox)c;
				if (chk.Checked)
					AddValue(c, "[X] ");
				else
					AddValue(c, "[ ] ");
			}
			else if (c is HtmlInputText) {
				AddValue(c, ((HtmlInputText)c).Value);
			}
			else if (c is HtmlTextArea) {
				AddValue(c, ((HtmlTextArea)c).Value);
			}
			else if (c is HtmlInputRadioButton) {
				HtmlInputRadioButton chk = (HtmlInputRadioButton)c;
				if (chk.Checked)
					AddValue(c, "[X] ");
				else
					AddValue(c, "[ ] ");
			}
			else if (c is CheckBox) {
				CheckBox chk = (CheckBox)c;
				if (chk.Checked)
					AddValue(c, "[X] " + chk.Text + " ");
				else
					AddValue(c, "");
			}
			else if (c is Button || c is HtmlInputButton || c is ImageButton || c is BaseValidator || c is LinkButton) {
				c.Visible = false;
			}
		}
	}
	// Swaps back the original controls
	private void SwapOriginalControls() {
		foreach (Control c in templateFormControls) {
			c.Visible = true;
		}
		foreach (Control c in controlValues) {
			c.Parent.Controls.Remove(c);
		}
	}
	private string GetEmailBodyFromControl(Control ctrl) {

		SwapValues();
		FileInfo template;
		StreamReader sr;
		if (!String.IsNullOrEmpty(_templatefile)) {
			template = new FileInfo(Server.MapPath(_templatefile));
			sr = template.OpenText();
			string templateContent = sr.ReadToEnd();
			sr.Close();
			if (templateContent.IndexOf("**EMAIL_BODY**") > -1) {
				templateContent = templateContent.Replace("**EMAIL_BODY**", RenderControl(ctrl));
			}
			if (!String.IsNullOrEmpty(_embedCSSfile)) {
				template = new FileInfo(Server.MapPath(_embedCSSfile));
				sr = template.OpenText();
				templateContent = templateContent.Replace("</head>", @"<style type=""text/css"">"
					+ sr.ReadToEnd() + "</style></head>");
				sr.Close();
			}
			SwapOriginalControls();
			
			return templateContent;
		}
		else {
			StringBuilder sb = new StringBuilder();
			
			sb.Append("<html><head><title>Form results</title>");
			sb.Append(@"<style type=""text/css"">
				body, input, textarea, select, td { 	
					font-family: Verdana, Arial, Helvetica, sans-serif;
					font-size: 12px;
				}");
			if (!String.IsNullOrEmpty(_embedCSSfile)) {
				template = new FileInfo(Server.MapPath(_embedCSSfile));
				sr = template.OpenText();
				sb.Append(sr.ReadToEnd());
				sr.Close();
			}
			sb.Append("</style></head><body>");
			sb.Append(RenderControl(ctrl));
			sb.Append("</body></html>");
			
			SwapOriginalControls();

			return sb.ToString();
		}

	}
	private string RenderControl(Control ctrl) {
		try {

			StringBuilder sb = new StringBuilder();
			StringWriter tw = new StringWriter(sb);
			HtmlTextWriter hw = new HtmlTextWriter(tw);

			ctrl.RenderControl(hw);
			return sb.ToString();
		}
		catch (Exception ex) {
            throw new HttpException(ex.Message);
            //throw new HttpException("FormMail: Can't create email body.  (Do you have EnableEventValidation=\"false\" in the page?)");
            
		}
	}
  
	private Control FindControlRecursively(Control root, string id) {
		Control current = root;

		if (current.ID == id)
			return current;

		foreach (Control control in current.Controls) {
			Control found = FindControlRecursively(control, id);
			if (found != null)
				return found;
		}
		return null;
	}
	protected void Page_Load(object sender, EventArgs e) {
		if (!String.IsNullOrEmpty(_submitButtonID)) {
			
			Control ctl = new Control();

			ctl = ((Control)this.Parent.FindControl(_submitButtonID));
			if (ctl == null) {
				ctl = (Control)FindControlRecursively(this.Page, _submitButtonID);
				if (ctl == null) {
					throw new HttpException("FormMail: Cannot find control '" + _submitButtonID + "'.");
				}
			}
						
			if (ctl != null) {
				if (ctl is Button) {
					((Button)ctl).Click += new EventHandler(b_Click);
				}
				else if (ctl is LinkButton) {
					((LinkButton)ctl).Click += new EventHandler(b_Click);
				}
				else if (ctl is HtmlInputButton) {
					((HtmlInputButton)ctl).ServerClick += new EventHandler(b_Click);
				}
				else if (ctl is ImageButton) {
					((ImageButton)ctl).Click += new ImageClickEventHandler(b_Click);
				}
			}
		}

	}

	void b_Click(object sender, EventArgs e) {
		bool safelength = true;
		if(_maxfilelength > 0) {
			foreach (string key in Request.Files) {
				if (Request.Files[key].ContentLength > (_maxfilelength * 1000)) {
					safelength = false;
				}
			}
		}

		Page.Validate();
		if (Page.IsValid && safelength) {
			SendEmail();
		}
		else if (!safelength) {
			errorMessages.Add("Uploaded files exceed the maximum filesize of " +
				_maxfilelength + " Kilobytes.");
			debugMessages.Add("Uploaded files exceed the maximum filesize of " +
				_maxfilelength + " Kilobytes.");
		}
		if (!debugMode) {
			if (errorMessages.Count > 0) {
				Panel pnl = new Panel();
				pnl.CssClass = ErrorCssClass;

				foreach (string error in errorMessages) {
					Label lbl = new Label();
					lbl.CssClass = ErrorCssClass;
					lbl.Text = error;
					pnl.Controls.Add(lbl);
				}
				controlOutput.Controls.Add(pnl);				
			}
		}
		else {
			Panel pnl = new Panel();
			pnl.CssClass = ErrorCssClass;
			pnl.Controls.Add(new LiteralControl("<ul>\n"));
			foreach (string message in debugMessages) {
				pnl.Controls.Add(new LiteralControl("<li>" + message + "</li>\n"));
			}
			pnl.Controls.Add(new LiteralControl("</ul>\n"));
			controlOutput.Controls.Add(pnl);			
		}

	}	
</script>
<asp:PlaceHolder ID="controlOutput" runat="server" />


