(function ($) {

    var app = function () {
        this.body = $('body');

        //CREATE FUNCTIONS FOR THE SITE
        var functionName = function () {

            var inputCellPhone = document.querySelector('#txtCellPhone');
            var inputOfficePhone = document.querySelector('#txtOfficePhone');
            var inputHomePhone = document.querySelector('#txtHomePhone');

            window.intlTelInput(inputCellPhone, {
                // any initialisation options go here
            });

            window.intlTelInput(inputOfficePhone, {
                // any initialisation options go here
            });

            window.intlTelInput(inputHomePhone, {
                // any initialisation options go here
            });
        };

        //THEN ADD THEM TO THE RUN FUNCTION
        var run = function () {
            functionName();
        };

        run();
    };

    $(function () {
        app();
    });
})(jQuery);