﻿// Functions specific to a site.

(function($) {
	$(document).ready(function() {
		// Place document manipulation code here
	    setclassfunction();
	}); 
})(jQuery);

// Original code by Stefano J. Attardi, MIT license



//this is for  setup ourpeople nav
function setclassfunction() {
   
   
    var currlocation = window.location.href.toLocaleLowerCase();
   
    if (currlocation.indexOf("/our-people") >= 0)
    {
        if (currlocation.indexOf("filter=leadership") >= 0) {
            $('.Our.People > ul > li.Directors').addClass("active");
            $('#people-filter > .button-group .leadership ').addClass("on");
             $('#people-filter > .button-group .leadership ').parent().addClass("on");

        }
        else if (currlocation.indexOf("filter=sales-trading") >= 0) {
            $('.Our.People > ul > li.Sales.Trading').addClass("active");
            $('#people-filter > .button-group .sales-trading ').addClass("on");
             $('#people-filter > .button-group .sales-trading ').parent().addClass("on");

        }
        else if (currlocation.indexOf("filter=research-analysts") >= 0) {
            $('.Our.People > ul > li.Research.Analysts').addClass("active");
            $('#people-filter > .button-group .research-analysts ').addClass("on");
            $('#people-filter > .button-group .research-analysts ').parent().addClass("on");

        }
        else if (currlocation.indexOf("filter=investment-bankers") >= 0) {
            $('.Our.People > ul > li.Investment.Bankers').addClass("active");
            $('#people-filter > .button-group .investment-bankers ').addClass("on");
            $('#people-filter > .button-group .investment-bankers ').parent().addClass("on");

        }

        else 
        {
            $('.Our.People > ul > li.All').addClass("active");
            $('#people-filter > .button-group .all ').addClass("on");
            $('#people-filter > .button-group .all ').parent().addClass("on");
        
        }
    
    }

  
        var $select = $('<select>').one().prependTo('#people-filter');
        $('#people-filter > ul > li').each(function() {
            var $li    = $(this),
                $a     = $li.find('> a'),
                $p     = $li.parents('li'),
                prefix = new Array($p.length + 1).join('-');

                // if($a.hasClass('on')){
                //   $li.addClass('on');
                // }

            var $option = $('<option>')
                .text(prefix + ' ' + $a.text())
                .val($a.attr('href'))                       
                .appendTo($select);

              if ($li.hasClass('on')) {
                  $option.attr('selected', 'selected');
              }
        });


        $select.on('change',function(){
          window.location.href = this.value;
        })


};