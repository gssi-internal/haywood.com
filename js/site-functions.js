﻿// Functions specific to a site.

(function($) {
	$(document).ready(function() {
		// Place document manipulation code here


		if($('.trans-items').hasClass('rptlist')){
			$('.view .listView').addClass('on');
		}
		if($('.trans-items').hasClass('rpGrid')){
			$('.view .gridView').addClass('on');
		}


		//Superslides
	    //  if ($('#home-slides').length) {
	    
	    
	    $('#home-slides').superslides({
				hashchange: false,
				animation: 'fade'
			});

		
		// GLOBAL VARIABLES
		var $html = $('html'),
			$body = $('body');		

		// trigger menu
		var $menuaction = $('.trigger.menu-action');
		$menuaction.on('click', function(){
			$body.removeClass('quickfind-action-on search-action-on-m');
			$body.toggleClass('menu-action-on-m');					
		});
		if ($('.rptlist #transactionsList').length) {
		    

		    var th_data;
		    $('#transactionsList').each(function () {
		        
		        var _table = $(this);
		      
		        $(this).find('.th-header > div').each(function (index) {
		            var index_th = index;
		            th_data = $(this).text();
		       
		            _table.find('li').each(function () {
		                $(this).find('div').each(function (index) {
		                    var index_td = index;
		                    if (index_th == index_td) {
		                        $(this).attr('data-content', th_data);
		                    }
		                });
		            });
		        });
		    });
		}

		//Util
		$('#util-search').on('click', function () {

		   // if ($(this).attr('id') != "utilloginusa") {
		        //if ($(this).attr('id') == "util-contact") {
		        //    window.location.replace("//haywood.com/who-we-are/locations");
		        //} else if ($(this).attr('id') == "utillogin") {
		        //    window.location.replace("//clients.haywood.com/clientcentre/member/default.aspx");
		        //} else if ($(this).attr('id') == "utilloginusa") {
		        //    window.location.replace("//clients.haywood.com/clientcentre/member/default.aspx");
		        //} else if ($(this).attr('id') == "header1_utilLogin") {
		        //    window.location.replace("//clients.haywood.com/clientcentre/member/default.aspx");
		        //}

		        //else {

		            var theUtil = $(this).attr('id');

		            theUtil = '#' + theUtil + '-wrap';

		            $('.util-top > div').hide();


		            if ($('#util').hasClass('open') && !$(this).hasClass('active')) {

		                $(theUtil).show();
		                $('#util li a').removeClass('active');
		                $(this).toggleClass('active');


		            } else if ($(this).hasClass('active')) {
		                $('#util').removeClass('open');
		                $('#util li a').removeClass('active');
		                $('#btn-util-close').removeClass('active');

		            } else {
		                $(theUtil).show();
		                $('#util').addClass('open');
		                $(this).addClass('active');
		                $('#btn-util-close').addClass('active');

		            }

		            $('.custom-search-input').focus();
		            $('.custom-search-input').select();
		      //  }
		  //  }
			
		});
		

		$('#btn-util-close').on('click', function () {

		    $('.custom-search-input').focus();
		    $('.custom-search-input').select();
			$(this).toggleClass('active');
			$('#util li a').removeClass('active');
			$('#util').removeClass('open');
		});



		
		//sitemap: add span element to navigation items with children
		var $navLinks = $('#foot-sitemap a');
		$navLinks.each(function(){
			$this = $(this);
			if ($this.next().length > 0) {
				$('<div class="togglemenu desktop-h"><span></span></div>').insertAfter($this);
			};
		});

		// toggle menu
		var $toggleM = $('.togglemenu, #foot-sitemap ul li h3');
		$toggleM.on('click', function(){
			$(this).parent().toggleClass('open');
		});

		//trigger
		$trigger = $('.trigger');
		$trigger.on('click', function(){
			$this = $(this);
			if($this.parent().hasClass('trigger-on')){
				$this.parent().toggleClass('trigger-on');
			}else{
				$trigger.parent().removeClass('trigger-on');
				$this.parent().toggleClass('trigger-on');
			}
			
		})


		//login input labels
		var $inputField = $('.login .field .input');

		$inputField.focus(function() {
			$(this).parent().addClass('focus');
		}).blur(function(){
			if( !$(this).val() ){
				$(this).parent().removeClass('focus');
			}
				
		});

		// lazy loading images
		$("img.lazy").lazyload();



		
		
		//$("#newsroom-content  ul.newsroom-list  li:nth-child(n+4) ").appendTo("#newsheadlines  ul");

		if ($('body.broadridge').length) {
		    $('#searchBtn').click(function () {
		        var searchValue = $('#search').val();
		        window.location.href = "//haywood.com/searchresults?indexCatalogue=everything&searchQuery=" + searchValue + "&wordsMode=0";
		    });
		}
        
	    // Location Selector - Popup
		if ($('.loc-ca').length) {

		    // set cookies on cick

		    $('.loc-ca').click(function () {
		        $.cookie("location", "ca", { path: '/' });
		        $(this).hide();
		        $('.loc-us').hide();
		        $('.loc-loader').show();
		        $('.locpop h2').after('<p>You have selected the country <span>Canada</span></p>');
		        window.location = "/";
		    });
		    $('.loc-us').click(function () {
		        $.cookie("location", "us", { path: '/' });
		        // $("#locationPopup").fancybox.close();

		        $(this).hide();
		        $('.loc-ca').hide();
		        $('.loc-loader').show();
		        $('.locpop h2').after('<p>You have selected the country <span>USA</span></p>');
		        window.location = "/usa";

		    });
		   

		    if ($.cookie('location') == 'undefined' || $.cookie('location') == '' || $.cookie('location') == null || $.cookie('location') == "ca") {
		        if ($('.sfPageEditor').length == 0) {
		          
		        }
		    }

		 
		    

		}


		$('.Our.People > ul').attr("data-filter-group", 'main');
		$('.Our.People > ul').addClass("button-group");

		$('.Our.People  li.Leadership a').attr("data-filter", '.leadership');

		$('.Our.People  li.Leadership a').attr("data-filter", '.leadership');
		$('.Our.People  li.Leadership a').attr("data-filter", '.leadership');

		$('.Our.People  li.Sales.Trading a').attr("data-filter", '.sales-trading');
		$('.Our.People  li.Research.Analysts a').attr("data-filter", '.research-analysts');
		
		$('.Our.People  li.Investment.Bankers a').attr("data-filter", '.investment-bankers');

	    /* Changing the tabular data into rows */
		

	}); 
})(jQuery);

// Original code by Stefano J. Attardi, MIT license

(function ($) {
    function toggleLabel() {
        var input = $(this);

        if (!input.parent().hasClass('placeholder')) {
            var label = $('<label>').addClass('placeholder');
            input.wrap(label);

            var span = $('<span>');
            span.text(input.attr('placeholder'))
            input.removeAttr('placeholder');
            span.insertBefore(input);
        }

        setTimeout(function () {
            var def = input.attr('title');
            if (!input.val() || (input.val() == def)) {
                input.prev('span').css('visibility', '');
                if (def) {
                    var dummy = $('<label></label>').text(def).css('visibility', 'hidden').appendTo('body');
                    input.prev('span').css('margin-left', dummy.width() + 3 + 'px');
                    dummy.remove();
                }
            } else {
                input.prev('span').css('visibility', 'hidden');
            }
        }, 0);
    };

    function resetField() {
        var def = $(this).attr('title');
        if (!$(this).val() || ($(this).val() == def)) {
            $(this).val(def);
            $(this).prev('span').css('visibility', '');
        }
    };

    var fields = $('input[type="search"]');

    fields.live('mouseup', toggleLabel); // needed for IE reset icon [X]
    fields.live('keydown', toggleLabel);
    fields.live('paste', toggleLabel);
    fields.live('focusin', function () {
        $(this).prev('span').css('color', '#ccc');
    });
    fields.live('focusout', function () {
        $(this).prev('span').css('color', '#999');
    });

    $(function () {
        $('input[placeholder], textarea[placeholder]').each(
            function () { toggleLabel.call(this); }
        );
    });

})(jQuery);