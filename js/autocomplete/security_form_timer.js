﻿var IDLE_TIMEOUT = 900; //seconds
var _idleSecondsTimer = null;
var _idleSecondsCounter = 0;

document.onclick = function () {
    _idleSecondsCounter = 0;
};

document.onmousemove = function () {
    _idleSecondsCounter = 0;
};

document.onkeypress = function () {
    _idleSecondsCounter = 0;
};

_idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);

function CheckIdleTime() {
    _idleSecondsCounter++;
    var oPanel = document.getElementById("SecondsUntilExpire");
    if (oPanel)
        oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
    if (_idleSecondsCounter >= IDLE_TIMEOUT) {
        window.clearInterval(_idleSecondsTimer);
        var curbody = document.body;

        curbody.classList.add("blur-body");
        document.getElementById("session_time_out").style.visibility = "visible";

        //alert("We are sorry.  Your session has expired. Please fill in the form again.");
        //document.location.href = "/security-form";
    }
}
