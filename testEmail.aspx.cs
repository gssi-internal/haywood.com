﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;

public partial class testEmail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_send_Click(object sender, EventArgs e)
    {
        SmtpClient smtpClient = new SmtpClient();

        MailAddress fromAddress = new MailAddress(txt_from.Text.Trim());
        MailMessage message = new MailMessage();

        //Response.Write("" + smtpClient.Host);
        //Response.End();
        smtpClient.Host = txt_server.Text.Trim();
        smtpClient.Port = int.Parse(txt_port.Text.Trim());

        message.From = fromAddress;
        message.To.Add(txt_to.Text.Trim());
        //message.Bcc.Add(txt_bcc.Text.Trim());
        message.Subject = "GSSI Test Email";
        message.IsBodyHtml = true;

        message.Body = "This is a test email.";

        System.Net.NetworkCredential smtpInfo = new System.Net.NetworkCredential(txt_username.Text.Trim(), txt_password.Text.Trim());
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Credentials = smtpInfo;

        try
        {
            smtpClient.Send(message); // Send a SMTP mail
            Response.Write("Success.");
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
            //Response.End();
        }
    }
}