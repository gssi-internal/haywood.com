<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchboxTemplate.ascx.cs" Inherits="UserControls_GSSI_SearchboxTemplate" %>

<ul>
    <li><a href="#" id="util-search">Search</a></li>
    <li><a href="/who-we-are/locations" id="util-contact">Contact</a>
        <a href="/usa/contact-us" id="util-contact-usa">Contact</a>
    </li>
    <li>
        
        <a href="//clients.haywood.com/clientcentre/member/default.aspx" id="utillogin">Client Centre</a>

         <a href="https://haywood.com/usa" id="utilloginusa" target="_blank">Client Centre</a>
      
    </li>
</ul>
<a href="#" id="btn-util-close">Close</a>
<div class="util-top">
    <asp:PlaceHolder ID="phCanadaContact" runat="server">
        <div id="util-contact-wrap"><a href="/who-we-are/locations">locate an office near you</a> </div>
        </asp:PlaceHolder>
    <asp:PlaceHolder ID="phUsaContact" runat="server" Visible="false">
        <div id="util-contact-wrap"><a href="/usa/contact-us">locate an office near you</a> </div>
        </asp:PlaceHolder>
        <div id="util-search-wrap">
            <asp:TextBox ID="searchTextBox" runat="server" CssClass="custom-search-input" TabIndex="1" type="search" name="search" placeholder="enter what you're looking for" autofocus  />
            <asp:Button ID="searchButton" runat="server" Text="Search" OnClientClick="return false;" CssClass="submitbtn" />
        </div>
    <asp:PlaceHolder ID="phCanadaRegister" runat="server">
        <div id="utillogin-wrap">
            <div class="passwords"><a href="/forgot-password">Forgot Password</a> <span>Not Registered?</span> <a href="/register-now">Register Now</a> </div>
            <input name="remember" type="checkbox" />
            <label for="remember">Remember me</label>
            <input name="username" class="input" type="text" placeholder="Name" />
            <input name="password" class="input" type="password" placeholder="Password" />
            <input name="login" type="submit" value="log in" />
        </div>
    </asp:PlaceHolder>
    
      
    <asp:PlaceHolder ID="phUsaRegister" runat="server" Visible="false">
        <div id="utillogin-wrap">
            <div class="passwords"><a href="/forgot-password">Forgot Password</a> <span>Not Registered?</span> <a href="/register-now">Register Now</a> </div>
            <input name="remember" type="checkbox" />
            <label for="remember">Remember me</label>
            <input name="username" class="input" type="text" placeholder="Name" />
            <input name="password" class="input" type="password" placeholder="Password" />
            <input name="login" type="submit" value="log in" />
        </div>
        </asp:PlaceHolder>
    
</div>




