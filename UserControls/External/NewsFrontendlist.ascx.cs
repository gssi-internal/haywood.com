﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_External_NewsFrontendlist : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string getlistimage()
    {

        List<Telerik.Sitefinity.Model.IDataItem> list1 = (List<Telerik.Sitefinity.Model.IDataItem>)Eval("Image");

        Telerik.Sitefinity.Libraries.Model.Image relatedimage1 = (Telerik.Sitefinity.Libraries.Model.Image)list1[0];

        return relatedimage1.Url;

    }

}