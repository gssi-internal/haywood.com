﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="peoplelistlayoutimprove.ascx.cs" Inherits="UserControls_External_peoplelistlayoutimprove" %>


<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.PublicControls.BrowseAndEdit" Assembly="Telerik.Sitefinity" %>

<telerik:RadListView
        id="listsControl"
        runat="server"
        ItemPlaceholderId="ListContainer"
        EnableEmbeddedSkins="false"
        EnableEmbeddedBaseStylesheet="false">
    <LayoutTemplate>

          <!-- BEGIN OUR_PEOPLE -->
               	<div id="people-filter">
	               		<ul class="button-group" data-filter-group="main">
	               			<li><a data-filter="" href="#" class="on">All</a></li>
	               			<li><a data-filter=".leadership" href="#">Leadership</a></li>
	               			<li><a data-filter=".sales-trading" href="#">Sales & Trading</a></li>
	               			<li><a data-filter=".research-analysts" href="#">Research Analysts</a></li>
	               			<li><a data-filter=".investment-bankers" href="#">Investment Bankers</a></li>
	               		</ul>
                    <ul class="button-group" data-filter-group="branch">
                      <li><a data-filter="" href="#" class="on">Filter By Branches</a></li>
                      <li><a data-filter="" href="#" class="on">All</a></li>
                      <li><a data-filter=".vancouver" href="#">Vancouver</a></li>
                      <li><a data-filter=".calgary" href="#">Calgary</a></li>
                      <li><a data-filter=".toronto" href="#">Toronto</a></li>
                    </ul>
                    <ul class="button-group" data-filter-group="s-t-group">
                      <li><a data-filter="" href="#" class="on">Filter By Group</a></li>
                      <li><a data-filter="" href="#" class="on">All</a></li>
                      <li><a data-filter=".institutional-sales" href="#">Institutional Sales</a></li>
                      <li><a data-filter=".retail-sales" href="#">Retail Sales</a></li>
                      <li><a data-filter=".portfolio-managers" href="#">Portfolio Managers</a></li>
                    </ul>
               	</div>
               	<div id="people-viewer">
                  <div class="viewer-wrapper">
               			<span class="close">close</span>
               			<div class="content"></div>
                  </div>
               	</div>
               	
               	<%--<div id="people-list"   >
               		<div class="section-inner">
               		<ul>
               			<li class="item leadership sales-trading">
               				<div class="thumb">
               					<img src="/img/tara.jpg" alt="green ranger" />
               				</div>
               				<div class="info">
               					<h3>Green Ranger</h3>
               					<em>Employee Position</em>
               				</div>
               				<div class="story">
               					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum sapien, tempus eget urna eu, commodo viverra felis. Sed metus quam, elementum a enim a, iaculis convallis urna. In finibus augue mi, quis tincidunt ipsum sagittis non.
               				</div>
               			</li>
               			<li class="item sales-trading research-analysts">
               				<div class="thumb">
               					<img src="/img/tara.jpg" alt="optimus prime" />
               				</div>
               				<div class="info">
               					<h3>Optimus Prime</h3>
               					<em>Employee Position</em>
               				</div>
               				<div class="story">
               					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum sapien, tempus eget urna eu, commodo viverra felis. Sed metus quam, elementum a enim a, iaculis convallis urna. In finibus augue mi, quis tincidunt ipsum sagittis non.
               				</div>
               			</li>
               			<li class="item research-analysts investment-bankers">
               				<div class="thumb">
               					<img src="/img/tara.jpg" alt="optimus prime" />
               				</div>
               				<div class="info">
               					<h3>Michelle Tanner</h3>
               					<em>Employee Position</em>
               				</div>
               				<div class="story">
               					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum sapien, tempus eget urna eu, commodo viverra felis. Sed metus quam, elementum a enim a, iaculis convallis urna. In finibus augue mi, quis tincidunt ipsum sagittis non.
               				</div>
               			</li>
               			<li class="item investment-bankers">
               				<div class="thumb">
               					<img src="/img/tara.jpg" alt="optimus prime" />
               				</div>
               				<div class="info">
               					<h3>Dee Jay Tanner</h3>
               					<em>Employee Position</em>
               				</div>
               				<div class="story">
               					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum sapien, tempus eget urna eu, commodo viverra felis. Sed metus quam, elementum a enim a, iaculis convallis urna. In finibus augue mi, quis tincidunt ipsum sagittis non. 
               				</div>
               			</li>
               			<li class="item sales-trading">
               				<div class="thumb">
               					<img src="/img/tara.jpg" alt="optimus prime" />
               				</div>
               				<div class="info">
               					<h3>Jesse Katsopolis </h3>
               					<em>Employee Position</em>
               				</div>
               				<div class="story">
               					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum sapien, tempus eget urna eu, commodo viverra felis. Sed metus quam, elementum a enim a, iaculis convallis urna. In finibus augue mi, quis tincidunt ipsum sagittis non.
               				</div>
               			</li>
               			<li class="item sales-trading research-analysts">
               				<div class="thumb">
               					<img src="/img/tara.jpg" alt="optimus prime" />
               				</div>
               				<div class="info">
               					<h3>Dan Villanueva</h3>
               					<em>Employee Position</em>
               				</div>
               				<div class="story">
               					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum sapien, tempus eget urna eu, commodo viverra felis. Sed metus quam, elementum a enim a, iaculis convallis urna. In finibus augue mi, quis tincidunt ipsum sagittis non.
               				</div>
               			</li>
               		</ul>
               		</div>
               	</div>--%>
                 
               


       <div   id="people-list"  class="">
                <asp:PlaceHolder id="ListContainer" runat="server" />
        </div>


        	<!-- END OUR_PEOPLE -->
    </LayoutTemplate>
    <ItemTemplate>
       
     
        <telerik:RadListView
                ID="listItemsControl"
                runat="server"
                ItemPlaceholderID="ItemsContainer"
                EnableEmbeddedSkins="false"
                EnableEmbeddedBaseStylesheet="false">
            <LayoutTemplate>
                <ul class="">
                    <asp:PlaceHolder ID="ItemsContainer" runat="server" />

                </ul>
            </LayoutTemplate>
            <ItemTemplate>

            
                <li   id="listitem"  class=" <%# getlistclass() %>   item " data-sf-provider='<%# Eval("Provider.Name")%>'  data-sf-id='<%# Eval("Id")%>' data-sf-type="Telerik.Sitefinity.Lists.Model.ListItem" data-sf-field="Title" data-sf-ftype="ShortText">
                    

                    	<div class="thumb">
               					<img src="<%# getlistimage()  %>" alt="Image" />
                        	<img src="#" alt="optimus prime" />


               				</div>
               		    <div class="info">
               					<h3><asp:Literal runat="server" Text='<%# Eval("Title") %>' />
                                       
               					</h3>
               					<em><asp:Literal runat="server" Text='<%# Eval("Position") %>' /></em>

               			</div>
               			<div class="story">
               					
                               <asp:Literal runat="server" Text='<%# Eval("Story") %>' />

               			</div>
                    <span class="close">close</span>
                </li>

               

            </ItemTemplate>
        </telerik:RadListView>
    </ItemTemplate>
</telerik:RadListView>
<asp:PlaceHolder ID="socialOptionsContainer" runat="server" />

<script type="text/javascript" src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/js/jquery.ba-bbq.min.js"></script>
<script type="text/javascript" src="/js/jquery.waypoints.min.js"></script>
<script>



//functions
var $container = $('#people-list ul');
function hashLoad(){
    var filterValue = window.location.hash.substr(1)
    $('#people-list ul').isotope({ filter: filterValue });
}
function hashSelected(){
    $('#people-filter a, .Our.People ul li a').each(function(){
      var filterLinkValue = $( this ).attr('data-filter');

      if(filterLinkValue == window.location.hash.substr(1)){
        $('#people-filter a, .Our.People ul li a').removeClass('on');
        $(this).addClass('on');
      }else{

      }
    })
}
function wayPointInit(){
  var waypoints = $('#people-viewer').waypoint({
    handler: function(d) {
      if(d == 'down' && $('#people-list').innerHeight() >= 600){
        var subNavWidth = $('#subnav').innerWidth();
        var subNavOffsetPos = $('#subnav').offset();
        var peopleViewerWidth = $('#people-viewer').innerWidth();
        var peopleViewerOffsetPos = $('#people-viewer').offset();
        var peopleFilterWidth = $('#people-filter').innerWidth();
        var peopleFilterOffsetPos = $('#people-filter').offset();


        $('#people-filter').addClass('fixed').css({
          'width' : peopleViewerWidth,
          'left' : peopleFilterOffsetPos.left,
        })
        $('#people-viewer').addClass('fixed').css({
          'width' : peopleViewerWidth,
          'left' : peopleViewerOffsetPos.left,
        });
        $('#subnav').addClass('fixed').css({
          'left' : subNavOffsetPos.left,
          'width' : subNavWidth,
        })

      }else{
        $('#people-filter').removeClass('fixed').css({
          'width' : '',
        });
        $('#people-viewer').removeClass('fixed').css({
          'width' : '',
        });
        $('#subnav').removeClass('fixed').css({
          'left' : '',
          'width' : '',
        })

      }
    }  
  })    
}

function beastReadyWin(jQuery){
 

  //information click
  // $('#people-list li').on( 'click', function(){
  //   var content = $(this).html();
  //   // $('#people-viewer .section-inner').remove();
  //   $('#people-list li').removeClass('on');
  //   $(this).addClass('on');
  //   $('#people-viewer').addClass('on');
  //   $('#people-viewer .viewer-wrapper .content').html(content);
  // })
  
}

$(window).on('load', function () {
    $container.isotope({
    percentPosition: true,
    itemSelector: '.item',
    layoutMode: 'fitRows',
    });

    if($(window).innerWidth() > 959){
      $('#people-list li:nth-of-type(5n+1)').addClass('first');
    }else{
      $('#people-list li:nth-of-type(3n+1)').addClass('first');
    }    
});
$(window).bind('hashchange',function(){
    hashLoad();
    hashSelected();
});

$(function() {



  var $prevItems;
  $('#people-list').on('click','.item',function(){
    $prevItems = $(this).prevUntil('.first');
    $prevItems.insertBefore(this);
    $('#people-list li').removeClass('large');
    $(this).addClass('large');
    $container.isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
    $container.isotope('relayout');
  })
   //close infobox
  $('#people-list').on('click','.item .close',function(e){
      e.stopPropagation();
      $(this).parent().removeClass('large');
      $container.isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
      $container.isotope('relayout');
  })


  wayPointInit()
  hashLoad();
  hashSelected();
  $('#people-filter ul:not(#people-filter ul:first-child)').each(function() {
      var select = $(document.createElement('select')).attr('data-group',$(this).data('filter-group')).insertBefore($(this).hide());

      select.prepend("<option disabled></option>");
    
      $('>li a', this).each(function(i) {
          option = $(document.createElement('option')).appendTo(select).val($(this).data('filter')).html($(this).html());
      });
      
      $('#people-filter select').on('change', function() {

          var $buttonGroup = $(this).next('.button-group');
          var filterGroup = $buttonGroup.attr('data-filter-group');
          filters[filterGroup] = $('option:selected',this).val();

          var filterValue = '';
          for(var prop in filters){
            filterValue += filters[ prop ];
          }

          $container.isotope({ filter: filterValue });
          window.location.hash = '#';
          window.location.hash = '#'+ filterValue;

        // if(filterValue == window.location.hash.substr(1)){
        //   // $(this).parent().parent().find('.on').removeClass('on');
        //   $(this).addClass('on');
        // }         
      });

  });

  //filter click
  var filters = {}; 

  $('.Our.People ul li a').on('click',function(e){
      e.preventDefault();

    var $buttonGroup = $(this).parents('.button-group');
    var filterGroup = $buttonGroup.attr('data-filter-group');
    filters[filterGroup] = $(this).attr('data-filter');

    $container.isotope({ filter: filters[filterGroup] });

    window.location.hash = '#';
    window.location.hash = '#'+ filters[filterGroup];

    

    //isolate .sales-trading
    if(filters[filterGroup] == '.sales-trading'){
      $('#people-filter select[data-group=s-t-group]').addClass('shows');
      $('#people-filter select').val('1');
    }else{
      $('#people-filter select').removeClass('shows');
      $('#people-filter select').val('1');
    }

  })

  $('#people-filter a').on( 'click', function(e) {

    e.preventDefault();
    var $buttonGroup = $(this).parents('.button-group');
    var filterGroup = $buttonGroup.attr('data-filter-group');
    filters[filterGroup] = $(this).attr('data-filter');

    $container.isotope({ filter: filters[filterGroup] });

    window.location.hash = '#';
    window.location.hash = '#'+ filters[filterGroup];

    //isolate .sales-trading
    if(filters[filterGroup] == '.sales-trading'){
      $('#people-filter select[data-group=s-t-group]').addClass('shows');
      $('#people-filter select').val('1');
    }else{
      $('#people-filter select').removeClass('shows');
      $('#people-filter select').val('1');
    }


    if(filters[filterGroup] == window.location.hash.substr(1)){
        $(this).parent().parent().find('.on').removeClass('on');
        $(this).addClass('on');
    }

    if($('#people-viewer').hasClass('on')){
      $('#people-viewer .close').click();
    }
  });

  
  $('#people-viewer .close').on('click',function(){
    $('#people-viewer').removeClass('on');
  })



});
</script>	