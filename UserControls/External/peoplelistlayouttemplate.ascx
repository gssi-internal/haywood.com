﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="peoplelistlayouttemplate.ascx.cs" Inherits="UserControls_External_peoplelistlayouttemplate" %>


<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.PublicControls.BrowseAndEdit" Assembly="Telerik.Sitefinity" %>

<telerik:RadListView
        id="listsControl"
        runat="server"
        ItemPlaceholderId="ListContainer"
        EnableEmbeddedSkins="false"
        EnableEmbeddedBaseStylesheet="false">
    <LayoutTemplate>

          <!-- BEGIN OUR_PEOPLE -->
               	<div id="people-filter">
	               		<ul class="button-group" data-filter-group="main">
	               			<li><a data-filter="" href="#" class="on">All</a></li>
	               			<li><a data-filter=".leadership" href="#">Leadership</a></li>
	               			<li><a data-filter=".sales-trading" href="#">Sales & Trading</a></li>
	               			<li><a data-filter=".research-analysts" href="#">Research Analysts</a></li>
	               			<li><a data-filter=".investment-bankers" href="#">Investment Bankers</a></li>
	               		</ul>
                    <ul class="button-group" data-filter-group="branch">
                      <li><a data-filter="" href="#" class="on">Filter All By Branch</a></li>
                      <!-- <li><a data-filter="" href="#" class="on">All</a></li> -->
                      <li><a data-filter=".vancouver" href="#">Vancouver</a></li>
                      <li><a data-filter=".calgary" href="#">Calgary</a></li>
                      <li><a data-filter=".toronto" href="#">Toronto</a></li>
                    </ul>
                    <ul class="button-group" data-filter-group="s-t-group">
                      <li><a data-filter="" href="#" class="on">Filter All By Group</a></li>
                      <!-- <li><a data-filter="" href="#" class="on">All</a></li> -->
                      <li><a data-filter=".institutional-sales" href="#">Institutional Sales</a></li>
                      <li><a data-filter=".retail-sales" href="#">Retail Sales</a></li>
                      <li><a data-filter=".portfolio-managers" href="#">Portfolio Managers</a></li>
                    </ul>
               	</div>
               	<div id="people-viewer">
                  <div class="viewer-wrapper">
               			<span class="close">close</span>
               			<div class="content"></div>
                  </div>
               	</div>
               	
               	
                 
               


       <div   id="people-list"  class="">
                <asp:PlaceHolder id="ListContainer" runat="server" />
        </div>


        	<!-- END OUR_PEOPLE -->
    </LayoutTemplate>
    <ItemTemplate>
       
     
        <telerik:RadListView
                ID="listItemsControl"
                runat="server"
                ItemPlaceholderID="ItemsContainer"
                EnableEmbeddedSkins="false"
                EnableEmbeddedBaseStylesheet="false">
            <LayoutTemplate>
                <ul class="">
                    <asp:PlaceHolder ID="ItemsContainer" runat="server" />

                </ul>
            </LayoutTemplate>
            <ItemTemplate>

            
                      <li   id="listitem"  class=" <%# getlistclassdirect() %>   item " data-sf-provider='<%# Eval("Provider.Name")%>'  data-sf-id='<%# Eval("Id")%>' data-sf-type="Telerik.Sitefinity.Lists.Model.ListItem" data-sf-field="Title" data-sf-ftype="ShortText">
                   

                           <%--<div class="thumb thumbimagewrap"  id='<%#  getlistimage() %>' style="width:174px; height: 195px"   >--%>
               			
                              <div class="thumb "  >
               			
                                  <img src="<%# getlistimage()  %>" alt="Image" />

               				</div>
               		    <div class="info">
               					<h3><asp:Literal runat="server" Text='<%# Eval("Title") %>' />
                                       
               					</h3>
               					<em><asp:Literal runat="server" Text='<%# Eval("Position") %>' /></em>

               			</div>
               			<div class="story">
               					
                               <asp:Literal runat="server" Text='<%# Eval("Story") %>' />

               			</div>
                    <span class="close">close</span>
                </li>

               

            </ItemTemplate>
        </telerik:RadListView>
    </ItemTemplate>
</telerik:RadListView>
<asp:PlaceHolder ID="socialOptionsContainer" runat="server" />






<script type="text/javascript" src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/js/jquery.ba-bbq.min.js"></script>
<script type="text/javascript" src="/js/jquery.waypoints.min.js"></script>
<script>

//functions
var $container = $('#people-list ul');
function hashLoad(){
    var filters = {};
    var filterValue = window.location.hash.substr(1)
    $('#people-list ul').isotope({ filter: '*' });
    $('#people-list ul').isotope({ filter: filterValue });
    $('#people-list ul').isotope('layout');
}
function wayPointInit(){
  var waypoints = $('#people-viewer').waypoint({
    handler: function(d) {
      if(d == 'down' && $('#people-list').innerHeight() >= 600){
        var subNavWidth = $('#subnav').innerWidth();
        var subNavOffsetPos = $('#subnav').offset();
        var peopleViewerWidth = $('#people-viewer').innerWidth();
        var peopleViewerOffsetPos = $('#people-viewer').offset();
        var peopleFilterWidth = $('#people-filter').innerWidth();
        var peopleFilterOffsetPos = $('#people-filter').offset();


        $('#people-filter').addClass('fixed').css({
          'width' : peopleViewerWidth,
          'left' : peopleFilterOffsetPos.left,
        })
        $('#people-viewer').addClass('fixed').css({
          'width' : peopleViewerWidth,
          'left' : peopleViewerOffsetPos.left,
        });
        $('#subnav').addClass('fixed').css({
          'left' : subNavOffsetPos.left,
          'width' : subNavWidth,
        })

      }else{
        $('#people-filter').removeClass('fixed').css({
          'width' : '',
        });
        $('#people-viewer').removeClass('fixed').css({
          'width' : '',
        });
        $('#subnav').removeClass('fixed').css({
          'left' : '',
          'width' : '',
        })

      }
    }  
  })    
}

// flatten object by concatting values
function concatValues( obj ) {
  var value = '';
  for ( var prop in obj ) {
    value += obj[ prop ];
  }
  return value;
}


$(window).on('load', function () {
    hashLoad();
    //responsive check i can try to explain but i wont
    if($(window).innerWidth() > 959){
      $('#people-list li:nth-of-type(5n+1)').addClass('first');
    }else{
      $('#people-list li:nth-of-type(3n+1)').addClass('first');
    }
    //isolate .sales-trading
    if(window.location.hash.substr(1) == '.sales-trading'){
      $('#people-filter select[data-group=s-t-group]').addClass('shows');
    }else{
      $('#people-filter select').removeClass('shows');
    }

    var $theHash = window.location.hash.substr(1);
    var hashSplit = $theHash.split('.');
    if(hashSplit[1] == 'sales-trading'){
      $('#people-filter select[data-group=s-t-group]').addClass('shows');
    }

    var $theHash = window.location.hash.substr(1);
    var hashSplit = $theHash.split('.');

     $('#people-filter a[data-filter=".'+hashSplit[1]+'"]').trigger('click');
     // $('#people-filter select[data-group=branch]').val('.'+hashSplit[2]).trigger('change');
     $('#people-filter select[data-group=s-t-group]').val('.'+hashSplit[2]).trigger('change'); 

      if($.inArray('sales-trading',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".sales-trading"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".sales-trading"]').addClass('on');
      }
      else if($.inArray('investment-bankers',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".investment-bankers"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".investment-bankers"]').addClass('on');
      }
      else if($.inArray('leadership',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".leadership"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".leadership"]').addClass('on');
      }
      else if($.inArray('research-analysts',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".research-analysts"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".research-analysts"]').addClass('on');
      }
      else {
        $('.Our.People ul li a').removeClass('on');
       // $('.Our.People ul li:first-child a').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter li:first-child a').addClass('on');
      }                            

});

$(window).bind('hashchange',function(){
    hashLoad();

    //on first load set up according to url hash
    var $theHash = window.location.hash.substr(1);
    var hashSplit = $theHash.split('.');
    // setTimeout(function() {
    //   //$('#people-filter a[data-filter=".'+ hashSplit[1] +'"').addClass('on').trigger('click');
    //   $('#people-filter a[data-filter=".'+ hashSplit[1] +'"').addClass('on');
    //   $('.Our.People ul li a[data-filter=".'+ hashSplit[1] +'"').addClass('on');
    // },10);
    
    // if(hashSplit.length <= 1){

    //   $('#people-filter a').removeClass('on');
    //   $('#people-filter li:first-child a').addClass('on');
    //   $('.Our.People ul li a').removeClass('on');
    //   $('.Our.People ul li:first-child a').addClass('on');
    // }else{
    //   $('.Our.People ul li a').removeClass('on');
    //   $('.Our.People ul li a[data-filter=".'+hashSplit[1]+'"]').addClass('on');
    //   $('#people-filter a').removeClass('on');
    //   $('#people-filter a[data-filter=".'+hashSplit[1]+'"]').addClass('on');

                 
    // }
    if($.inArray('sales-trading',hashSplit) > -1 ){
      $('#people-filter a').removeClass('on');
      $('.Our.People ul li a[data-filter=".sales-trading"]').addClass('on');
      $('#people-filter a[data-filter=".sales-trading"]').addClass('on');
    } 
if($.inArray('sales-trading',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".sales-trading"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".sales-trading"]').addClass('on');
      }
      else if($.inArray('investment-bankers',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".investment-bankers"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".investment-bankers"]').addClass('on');
      }
      else if($.inArray('leadership',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".leadership"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".leadership"]').addClass('on');
      }
      else if($.inArray('research-analysts',hashSplit) > -1 ){
        $('.Our.People ul li a').removeClass('on');
        $('.Our.People ul li a[data-filter=".research-analysts"]').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter a[data-filter=".research-analysts"]').addClass('on');
      }
      else {
        $('.Our.People ul li a').removeClass('on');
       // $('.Our.People ul li:first-child a').addClass('on');
        $('#people-filter a').removeClass('on');
        $('#people-filter li:first-child a').addClass('on');
      } 
});

$(function() {

   var filters = {}; 

  //load images
  // $('.thumbimagewrap').each(function (index) {
  //     $(this).load("/ImageHandler.ashx?id=" + $.trim($(this).attr('id')),function(){
  //         $container.imagesLoaded(function(){
  //             $container.isotope({ //triggering isotope
  //                 itemSelector: '.item'
  //             });
  //         });
  //     });
  // }) 

  //wayPointInit()

  hashLoad();

  var $prevItems;
  $('#people-list').on('click','.item',function(){
    $prevItems = $(this).prevUntil('.first');
    $prevItems.insertBefore(this);
    $('#people-list li').removeClass('large');
    $(this).addClass('large');
    $container.isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
    $container.isotope('layout');
  })

   //close infobox
  $('#people-list').on('click','.item .close',function(e){
      e.stopPropagation();
      $(this).parent().removeClass('large');
      $container.isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
      $container.isotope('layout');
  })

  //swtich sub filters to select boxes
  $('#people-filter ul:not(#people-filter ul:first-child)').each(function() {
      var select = $(document.createElement('select')).attr('data-group',$(this).data('filter-group')).insertBefore($(this).hide());

      select.prepend("<option disabled></option>");
    
      $('>li a', this).each(function(i) {
          option = $(document.createElement('option')).appendTo(select).val($(this).data('filter')).html($(this).html());
      });
      

      $('#people-filter select').val('3');
      var $theHash = window.location.hash.substr(1);
      var hashSplit = $theHash.split('.');

      // if(hashSplit.length = 3){
      //   $('#people-filter select[data-group=branch] option[value=".'+hashSplit[2]+'"]').trigger('change');
      // }
      // if(hashSplit.length = 4){
      //   $('#people-filter select[data-group=s-t-group] option[value=".'+hashSplit[3]+'"]').trigger('change');
      // }
      
      $('#people-filter select').on('change', function() {

          var $buttonGroup = $(this).next('.button-group');
          var filterGroup = $buttonGroup.attr('data-filter-group');
          filters[filterGroup] = $('option:selected',this).val();

          var filterValue = '';
          for(var prop in filters){
            filterValue += filters[ prop ];
          }

          window.location.hash = '#'+ filterValue;

        // if(filterValue == window.location.hash.substr(1)){
        //   // $(this).parent().parent().find('.on').removeClass('on');
        //   $(this).addClass('on');
        // }         
      });

  });

  //filter click
  $('.Our.People ul li a').on('click',function(e){
    e.preventDefault();

    var $buttonGroup = $(this).parents('.button-group');
    var filterGroup = $buttonGroup.attr('data-filter-group');
    filters[filterGroup] = $(this).attr('data-filter');
    var filterValue = concatValues( filters );


    //change hash according to filtervalue
    window.location.hash = '#'+ filterValue;

    //reset filters ( hash ) when parent filter is clicked
    var $theHash = window.location.hash.substr(1);
    var hashSplit = $theHash.split('.');

    if(hashSplit.length>=3){
      window.location.hash = '#.' + hashSplit[1] + '.' + $(this).attr('data-filter');
      $('#people-filter select').val('#people-filter select option:eq(2)').trigger('change');
    }

    //isolate .sales-trading
    if(filters[filterGroup] == '.sales-trading'){
      $('#people-filter select[data-group=s-t-group]').addClass('shows');
    }else{
      $('#people-filter select').removeClass('shows');
    }
  });
 
  $('#people-filter').on( 'click','a', function(e) {

    e.preventDefault();
    
    var $buttonGroup = $(this).parent().parent('.button-group');
    var filterGroup = $buttonGroup.attr('data-filter-group');
    filters[filterGroup] = $(this).attr('data-filter');
    var filterValue = concatValues( filters );

    //change hash according to filtervalue
    window.location.hash = '#'+ filterValue;
   

    //reset filters ( hash ) when parent filter is clicked
    var $theHash = window.location.hash.substr(1);
    var hashSplit = $theHash.split('.');

    if(hashSplit.length>=3){
      window.location.hash = '#.' + hashSplit[1] + '.' + $(this).attr('data-filter');
      $('#people-filter select').val('#people-filter select option:eq(2)').trigger('change');
    }


    //show third filter only for sales trading
    if(filters[filterGroup] == '.sales-trading'){
      $('#people-filter select[data-group=s-t-group]').addClass('shows');
    }else{
      $('#people-filter select').removeClass('shows');
    }


  });

  
  // $('#people-viewer .close').on('click',function(){
  //   $('#people-viewer').removeClass('on');
  // })


  

});
</script>	