﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsFrontendlist.ascx.cs" Inherits="UserControls_External_NewsFrontendlist" %>




<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.ContentUI" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.Comments" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.PublicControls.BrowseAndEdit" Assembly="Telerik.Sitefinity" %>

<sf:SitefinityLabel id="title" runat="server" WrapperTagName="div" HideIfNoText="true" HideIfNoTextMode="Server" /> 
<telerik:RadListView ID="NewsList" ItemPlaceholderID="ItemsContainer" runat="server" EnableEmbeddedSkins="false" EnableEmbeddedBaseStylesheet="false">
    <LayoutTemplate>
     
         <div id="newsroom-content">
                  <h1>In The News</h1>
           
           
           <ul class=" newsroom-list  ">
            <asp:PlaceHolder ID="ItemsContainer" runat="server" />
        </ul>
           
           
         </div>
      
      
      
    </LayoutTemplate>
    <ItemTemplate>
        <li class="sfnewsListItem sflistitem" data-sf-provider='<%# Eval("Provider.Name")%>'  data-sf-id='<%# Eval("Id")%>' data-sf-type="Telerik.Sitefinity.News.Model.NewsItem">
            
    

                    <div class="thumb">
                        
               
                  <img src="<%# getlistimage()  %>" alt="the title" />
               
               
                        </div>
                        <span class="date"><sf:FieldListView ID="PublicationDate" runat="server" Format="{PublicationDate.ToLocal():MMM dd, yyyy}" />
            		</span>
                        <h2> <sf:DetailsViewHyperLink TextDataField="Title" ToolTipDataField="Description" data-sf-field="Title" data-sf-ftype="ShortText" runat="server" /></h2>
                        <p>
                          <sf:FieldListView ID="summary" runat="server" Text="{0}" Properties="Summary" WrapperTagName="div" WrapperTagCssClass="sfnewsSummary sfsummary" EditableFieldType="ShortText"/> 
          		</p>
                        <p> <sf:DetailsViewHyperLink ID="FullStory" Text="<%$ Resources:NewsResources, FullStory %>" runat="server" CssClass="sfnewsFullStory sffullstory" />
        
        		</p>

          
          
         

            

           
      
      </li>
    </ItemTemplate>
</telerik:RadListView>
<sf:Pager id="pager" runat="server"></sf:Pager>
<asp:PlaceHolder ID="socialOptionsContainer" runat="server" />

<%--   <div id="newsheadlines" class="headlines">
                    <h2>Other Headlines</h2>
                    <ul>
                     
                    
                    </ul>
 </div>--%>