﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

using Telerik.Sitefinity;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lists.Model;
using Telerik.Sitefinity.Workflow;

using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lists.Model;
using Telerik.Sitefinity.Modules.Lists;
using Telerik.Sitefinity.Workflow;
using Telerik.OpenAccess;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;

using Telerik.Sitefinity.Model;




public partial class UserControls_External_peoplelistlayouttemplate : System.Web.UI.UserControl
{

     public static string[] peoplelistclass = { "leadership", "sales-trading", "research-analysts", "investment-bankers", "Vancouver", "Calgary", "Toronto", "Institutional-Sales", "Retail-Sales", "portfolio-managers" };


    protected void Page_Load(object sender, EventArgs e)
    {
             

    }

    protected string getlistimage()
    {

        List<Telerik.Sitefinity.Model.IDataItem> list1 = (List<Telerik.Sitefinity.Model.IDataItem>)Eval("Image");
        Telerik.Sitefinity.Libraries.Model.Image relatedimage1 = (Telerik.Sitefinity.Libraries.Model.Image)list1[0];

      //  return relatedimage1.Id.ToString();
        return relatedimage1.Url;

    }

 

    protected string getlistclass( )
    {

        //if (category == null)
        //{

        //}

        string returnstring = "";

        // string[] peoplelistclass = { "leadership", "sales-trading", "research-analysis", "investment-bankers" };



        foreach (string tmppeopleclassstring in peoplelistclass)
        {


            try
            {



                TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
                var classCategory = taxonomyManager.GetTaxa<HierarchicalTaxon>().Where(t => t.Name == tmppeopleclassstring).FirstOrDefault();

                if (classCategory != null)
                {

                    Telerik.Sitefinity.Lists.Model.ListItem listitem1 = GetListItemByTitleFluentAPI(Eval("Id").ToString());


                    if (listitem1 != null)
                    {


                        if (


                       listitem1.Organizer.TaxonExists("Category", classCategory.Id)


                            )
                        {

                            returnstring += tmppeopleclassstring.ToLower() + "   ";

                        }

                        else
                        {


                        }

                    }




                }


            }

            catch (Exception e)
            {

                //  throw e;

            }



        }


        return returnstring;
     

    }

    protected string getlistclassdirect()
    {

        string returnstring = "";

        Telerik.Sitefinity.Lists.Model.ListItem listitem1 = GetListItemByTitleFluentAPI(Eval("Id").ToString());

        var manager = TaxonomyManager.GetManager();
        var categoriesTaxonomy = manager.GetTaxonomy<HierarchicalTaxonomy>(TaxonomyManager.CategoriesTaxonomyId);

        foreach (var itemcategory in listitem1.GetValue<TrackedList<Guid>>("Category"))
        {
            var classcategorys = manager.GetTaxa<HierarchicalTaxon>().Where(t => t.Id == itemcategory).FirstOrDefault();
            returnstring += classcategorys.Name + " ";

        }

        return returnstring;

    }



    public  Telerik.Sitefinity.Lists.Model.ListItem GetListItemByTitleFluentAPI(string myid)
    {
       

        Guid  mynewguid=new Guid(myid);

        Telerik.Sitefinity.Lists.Model.ListItem listItem = App.WorkWith().ListItems().Where(i => (i.Id ==mynewguid && i.Status == ContentLifecycleStatus.Live))
            .Get().FirstOrDefault();
      
       
        return listItem;
    }


}