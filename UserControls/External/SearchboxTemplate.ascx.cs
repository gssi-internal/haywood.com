﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_GSSI_SearchboxTemplate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string curl = Request.Url.AbsoluteUri.ToLower();
        if (curl.Contains("/usa"))
        {
          //  utillogin.NavigateUrl = "https://clientexp.swst.com/haywood/LogIn";
            phUsaContact.Visible = true; 
            phCanadaContact.Visible = false; phCanadaRegister.Visible = false;
        }
        else
        {
          //  utillogin.NavigateUrl = "#";
            phUsaContact.Visible = false; phUsaRegister.Visible = false;
            phCanadaContact.Visible = true; phCanadaRegister.Visible = true;
        }
    }
}