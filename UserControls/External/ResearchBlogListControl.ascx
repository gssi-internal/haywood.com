﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResearchBlogListControl.ascx.cs" Inherits="UserControls_External_ResearchBlogListControl" %>



<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.ContentUI" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.Comments" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.PublicControls.BrowseAndEdit" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="Telerik.Sitefinity" %>	

<sf:SitefinityLabel id="title" runat="server" WrapperTagName="div" HideIfNoText="true" HideIfNoTextMode="Server" />
<telerik:RadListView ID="Repeater" ItemPlaceholderID="ItemsContainer" runat="server" EnableEmbeddedSkins="false" EnableEmbeddedBaseStylesheet="false">
    <LayoutTemplate>

               <div id="research">
                   


                    <ul class="research-list">
                     <asp:PlaceHolder ID="ItemsContainer" runat="server" />
                    </ul>

                    </div>

       
    </LayoutTemplate>
    <ItemTemplate>

        

        <li class="sfpostListItem sflistitem" data-sf-provider='<%# Eval("Provider.Name")%>'  data-sf-id='<%# Eval("Id")%>' data-sf-type="Telerik.Sitefinity.Blogs.Model.BlogPost">
            

             <div class="thumb">
         <img src="<%# getlistimage()  %>" alt="the title" />
          </div>
          <h2  class="posttitle" > <sf:DetailsViewHyperLink TextDataField="Title" ToolTipDataField="Description" data-sf-field="Title" data-sf-ftype="ShortText" runat="server" /></h2>
          <p> <sf:FieldListView ID="PostContent" runat="server" Text="{0}" Properties="Summary" WrapperTagName="div" WrapperTagCssClass="sfpostSummary sfsummary" EditableFieldType="ShortText" />
          </p>
        
        <p> <sf:DetailsViewHyperLink ID="FullStory" Text="<%$ Resources:BlogResources, FullStory %>" runat="server" CssClass="sfpostFullStory sffullstory" /></p>



          

           
        </li>
    </ItemTemplate>
</telerik:RadListView>
<sf:Pager id="pager" runat="server"></sf:Pager>
<asp:PlaceHolder ID="socialOptionsContainer" runat="server" />