﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Telerik.Sitefinity; 
using Telerik.Sitefinity.Model; 
using Telerik.Sitefinity.DynamicModules; 
using Telerik.Sitefinity.Data.Linq.Dynamic; 
using Telerik.Sitefinity.DynamicModules.Model; 
using Telerik.Sitefinity.GenericContent.Model; 
using Telerik.Sitefinity.Utilities.TypeConverters; 
using Telerik.Sitefinity.Security; 
using Telerik.Sitefinity.Lifecycle; 

//For taxonomies include the following: 
using Telerik.Sitefinity.Taxonomies; 
using Telerik.Sitefinity.Taxonomies.Model; 

//For media type include the following: 
using Telerik.Sitefinity.Modules.Libraries; 

//For related data fields include the following: 
using Telerik.Sitefinity.RelatedData; 

//For address fields include the following: 
using Telerik.Sitefinity.Locations.Configuration; 
using Telerik.Sitefinity.GeoLocations.Model; 
using Telerik.Sitefinity.Configuration; 
using Telerik.Sitefinity.Locations;
using Telerik.OpenAccess;
using System.Data;
using Telerik.Sitefinity.Model.ContentLinks;
using System.Text;


public partial class UserControls_GSSI_Transactions : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the provider name for the DynamicModuleManager here. All available providers are listed in
        // Administration -> Settings -> Advanced -> DynamicModules -> Providers
        var providerName = String.Empty;
        DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
        Type transactionsType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Transactions.Transactions");
      //  CreateTransactionsItem(dynamicModuleManager, transactionsType);
     

        // This is how we get the collection of Transactions items
        var myCollection = dynamicModuleManager.GetDataItems(transactionsType).Where(p => p.Visible == true);
        // At this point myCollection contains the items from type transactionsType

        TrackedList<Guid> cats = new TrackedList<Guid>();
        ContentLink[] logoImage;

        DataTable tempTable = new DataTable("Products");
        tempTable.Columns.Add("Title", typeof(string));
        tempTable.Columns.Add("Type", typeof(string));
        tempTable.Columns.Add("Amount", typeof(string));
        tempTable.Columns.Add("Information", typeof(string));
        tempTable.Columns.Add("Date", typeof(string));
        tempTable.Columns.Add("Logo", typeof(string));
        tempTable.Columns.Add("Category", typeof(string));

        StringBuilder sb = new StringBuilder("");
        TaxonomyManager tManager = TaxonomyManager.GetManager();
        foreach (var item in myCollection)
        {
            DataRow nRow = tempTable.NewRow();
           //if (item.GetValue("Logo") != null)
           //{
           //    logoImage = (ContentLink[])item.GetValue("Logo");
           //    if (logoImage != null && logoImage.Length > 0)
           //        nRow["Logo"] = logoImage[0].ChildItemAdditionalInfo;
           //    else
           //        nRow["Logo"] = DBNull.Value;
           //}


    //        logoImage = (ContentLink[])item.GetValue("Logo");
            // Only getting the first image... 
            //if (link != null && link.Length > 0)
            //{
            //    var image = LibrariesManager.GetManager().GetImage(link[0].ChildItemId);
            //    //ImageUrl = image.Url;
            //}
        //

            nRow["Title"] = item.GetValue("Title");

           

            nRow["Type"] = item.GetValue("Type");
            nRow["Amount"] = item.GetValue("Amount");
            nRow["Information"] = item.GetValue("Information");
            nRow["Date"] = item.GetValue("Date");

            cats = item.GetValue<TrackedList<Guid>>("Category");
            sb = new StringBuilder("");
            

            if (cats != null && cats.Count > 0)
            {
                foreach (var catItem in cats)
                {

                    var catName = tManager.GetTaxon<HierarchicalTaxon>(catItem).Title;
                    sb.Append(catName.ToString());
                }

            }

            nRow["Category"] = sb.ToString();

            tempTable.Rows.Add(nRow);
        }
        rptlist.DataSource = tempTable.DefaultView;
        rptlist.DataBind();
    }




    
    // Creates a new transactions item
    private void CreateTransactionsItem(DynamicModuleManager dynamicModuleManager, Type transactionsType)
    {
        DynamicContent transactionsItem = dynamicModuleManager.CreateDataItem(transactionsType);

        // This is how values for the properties are set 
        transactionsItem.SetValue("Title", "Some Title");
        transactionsItem.SetValue("Type", "Some Type");
        transactionsItem.SetValue("Amount", "Some Amount");
        transactionsItem.SetValue("Information", "Some Information");
        transactionsItem.SetValue("Date", DateTime.Now);
        TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
        var Category = taxonomyManager.GetTaxa<HierarchicalTaxon>().Where(t => t.Taxonomy.Name == "Categories").FirstOrDefault();
        if (Category != null)
        {
            transactionsItem.Organizer.AddTaxa("Category", Category.Id);
        }

        LibrariesManager logoManager = LibrariesManager.GetManager();
        var logoItem = logoManager.GetImages().FirstOrDefault(i => i.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master);
        if (logoItem != null)
        {
            // This is how we relate an item
            transactionsItem.CreateRelation(logoItem, "Logo");
        }

        transactionsItem.SetString("UrlName", "SomeUrlName");
        transactionsItem.SetValue("Owner", SecurityManager.GetCurrentUserId());
        transactionsItem.SetValue("PublicationDate", DateTime.Now);
        transactionsItem.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, "Draft");

        // You need to call SaveChanges() in order for the items to be actually persisted to data store
        dynamicModuleManager.SaveChanges();
    }


    protected void LoadAllRecipesBackup(int pageindex)
    {
        DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
        Type transactionsType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Transactions.Transactions");

        // This is how we get the recipe items through filtering
        var myTransactions = dynamicModuleManager.GetDataItems(transactionsType).Where(i => i.Visible);

        // At this point myFilteredCollection contains the items that match the lambda expression passed to the Where extension method
        int index = 0;
        List<int> featuredIndexes = new List<int>();
        TrackedList<Guid> cats = new TrackedList<Guid>();
        List<Transactions> myFilteredTransactions = new List<Transactions>();
        foreach (var myTransaction in myTransactions)
        {
            Transactions t = new Transactions();
            t.Id = myTransaction.Id;
            t.Title = myTransaction.GetValue("Title").ToString();
            t.Type = myTransaction.GetValue("Type").ToString();
            t.Amount = myTransaction.GetValue("Amount").ToString();

            LibrariesManager libraryManager = LibrariesManager.GetManager();
            TaxonomyManager tManager = TaxonomyManager.GetManager();
            StringBuilder sb = new StringBuilder("");
            //   var picture = (ContentLink[])myTransaction.GetValue("Logo");
            //if (picture != null && picture.Length > 0)
            //    t.Logo = libraryManager.GetImage(picture.FirstOrDefault().ChildItemId).Url;

            //  Telerik.Sitefinity.Libraries.Model.Image image = (Telerik.Sitefinity.Libraries.Model.Image)myTransaction.GetValue("Logo");

            t.Information = myTransaction.GetValue("Information").ToString();
            t.Date = myTransaction.GetValue("Date").ToString();

            cats = myTransaction.GetValue<TrackedList<Guid>>("Category");
            sb = new StringBuilder("");
            if (cats != null && cats.Count > 0)
            {
                foreach (var catItem in cats)
                {

                    var catName = tManager.GetTaxon<HierarchicalTaxon>(catItem).Title;
                    sb.Append(catName.ToString());
                }

            }
            t.Category = sb.ToString();

            myFilteredTransactions.Add(t);
            index++;
        }


        if (index > 0)
        {

            rptlist.DataSource = myFilteredTransactions;
            rptlist.DataBind();

            rptlist.Visible = true;
        }

    }

    protected void lbtnSort_Click(object sender, EventArgs e)
    {
        DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
        Type transactionsType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Transactions.Transactions");

        // This is how we get the xypexVideo items through filtering
        var myTransactions = dynamicModuleManager.GetDataItems(transactionsType).Where(i => i.Visible);
        TaxonomyManager taxManager = TaxonomyManager.GetManager();
        var categories = taxManager.GetTaxonomies<FlatTaxonomy>().Where(t => t.Name.ToLower().Contains("recipe-categories")).SingleOrDefault();
        Taxon searchCat = null;
        foreach (Taxon cat in categories.Taxa)
        {
            if (cat.Name == drpSorting.SelectedValue)
                searchCat = cat;
        }

        // At this point myFilteredCollection contains the items that match the lambda expression passed to the Where extension method
        List<Transactions> myFilteredTransactions = new List<Transactions>();
        int featuredCnt = 0;
        int index = 0;
        List<int> featuredIndexes = new List<int>();
        TrackedList<Guid> cats = new TrackedList<Guid>();
        foreach (var myTransaction in myTransactions)
        {
            bool match = false;
            string mySearchIngredients = myTransaction.GetValue("SearchIngredients").ToString();
            match = (searchCat == null || myTransaction.Organizer.TaxonExists("Categories", searchCat.Id));


            if (match)
            {
                Transactions t = new Transactions();
                t.Id = myTransaction.Id;
                t.Title = myTransaction.GetValue("Title").ToString();
                t.Type = myTransaction.GetValue("Type").ToString();
                t.Amount = myTransaction.GetValue("Amount").ToString();

                LibrariesManager libraryManager = LibrariesManager.GetManager();
                TaxonomyManager tManager = TaxonomyManager.GetManager();
                StringBuilder sb = new StringBuilder("");
                //   var picture = (ContentLink[])myTransaction.GetValue("Logo");
                //if (picture != null && picture.Length > 0)
                //    t.Logo = libraryManager.GetImage(picture.FirstOrDefault().ChildItemId).Url;

                //  Telerik.Sitefinity.Libraries.Model.Image image = (Telerik.Sitefinity.Libraries.Model.Image)myTransaction.GetValue("Logo");

                t.Information = myTransaction.GetValue("Information").ToString();
                t.Date = myTransaction.GetValue("Date").ToString();

                cats = myTransaction.GetValue<TrackedList<Guid>>("Category");
                sb = new StringBuilder("");
                if (cats != null && cats.Count > 0)
                {
                    foreach (var catItem in cats)
                    {

                        var catName = tManager.GetTaxon<HierarchicalTaxon>(catItem).Title;
                        sb.Append(catName.ToString());
                    }

                }
                t.Category = sb.ToString();
                myFilteredTransactions.Add(t);
                index++;
            }
        }



        if (index > 0)
        {

            rptlist.DataSource = myFilteredTransactions;
            rptlist.DataBind();

            rptlist.Visible = true;
        }
        Session["LastSearch"] = "Sorting";
        Session["LastSortingCategory"] = drpSorting.SelectedIndex;





    }
}