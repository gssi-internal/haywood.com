﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecurityForm.ascx.cs" Inherits="UserControls_GSSI_SecurityForm" %>

<script type="text/javascript" src="/js/autocomplete/jquery-1.11.2.js"></script>
    <script type="text/javascript" src="/js/autocomplete/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/intlTelInput.min.js"></script>
    <script type="text/javascript" src="/js/utils.js"></script>

    <link href="/css/jquery-ui.css" rel="Stylesheet" />
    <link href="/css/intlTelInput.min.css" rel="Stylesheet" />
<link href="/css/security_form.css" rel="Stylesheet" />
<link rel="stylesheet" href="/css/chosen.css" />

<script type="text/javascript" src="/js/autocomplete/security_form_timer.js"></script>
<%--<script type="text/javascript" src="/js/form-functions.js"></script>--%>
<style>
    #session_time_out {
        /*height: 400px;*/
         visibility:hidden;
    }
    #content ul li::before {
        content: none;
    }
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadScriptManager runat="server" ID="RadScriptManager1" />

<div class="security-form">
    <div class="form-row" style="display:none;">
        
You will be auto logged out in <span id="SecondsUntilExpire"></span> seconds.
    </div>
    <div class="form-group">

        <div class="form-section-title tell-us-about-you">
        <span>1</span>
            <h3>Tell Us About Yourself</h3>
        </div>
            
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Full Legal Name:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtFullLegalName" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Marital Status:</span>
        </div>
        <div class="form-control">
            <asp:RadioButtonList runat="server" ID="radListMatrialStatus" RepeatDirection="Horizontal" ClientIDMode="Static">
                <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                
                <asp:ListItem Text="Single" Value="Single"></asp:ListItem>

                <asp:ListItem Text="Divorced" Value="Divorced"></asp:ListItem>

                <asp:ListItem Text="Widowed" Value="Widowed"></asp:ListItem>

                <asp:ListItem Text="Separated" Value="Separated"></asp:ListItem>

                <asp:ListItem Text="Common Law" Value="Common Law"></asp:ListItem>
                
            </asp:RadioButtonList>

        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Number of Dependents:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtNumberOfDependents" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" Width="100"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Address:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtAddress" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Address 2:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtAddress2" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Country:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtCountry" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Region (Province/State):</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtRegion" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>City:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtCity" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Postal/Zip Code:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtPostalCode" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Do you rent or own?</span>
        </div>
        <div class="form-control">

        <asp:RadioButtonList runat="server" ID="radRentOwn" RepeatDirection="Horizontal" ClientIDMode="Static">
                <asp:ListItem Text="Rent" Value="Rent"></asp:ListItem>
                <asp:ListItem Text="Own" Value="Own"></asp:ListItem>
            </asp:RadioButtonList>

        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Cell Phone #:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtCellPhone" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" onkeyup="process(event);"></asp:TextBox>        
            <asp:HiddenField runat="server" ID="hdCellPhoneNumber" ClientIDMode="Static" />
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Office #:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtOfficePhone" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" onkeyup="process(event);"></asp:TextBox>        
            <asp:HiddenField runat="server" ID="hdOfficePhone" ClientIDMode="Static" />
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Home Phone #:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtHomePhone" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" onkeyup="process(event);"></asp:TextBox>        
            <asp:HiddenField runat="server" ID="hdHomePhone" ClientIDMode="Static" />
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Date of birth:</span>
        </div>
        <div class="form-control" id="date-birth">
            <div id="birth-day">
                <span>Day</span>
                <asp:TextBox runat="server" ID="txtDateOfBirthDay" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" placeholder="DD"></asp:TextBox>        
            </div>
            <div id="birth-month">
                <span>Month</span>
                <asp:TextBox runat="server" ID="txtDateOfBirthMonth" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" placeholder="MM"></asp:TextBox>
            </div>

            <div id="birth-year">
                <span>Year</span>
                <asp:TextBox runat="server" ID="txtDateOfBirthYear" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" placeholder="YYYY"></asp:TextBox>
            </div>
            
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>SIN/SSN or Tax ID number if offshore:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtTaxID" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-section">

    <span>Employer’s Information:</span>

        <div class="form-group">
            <div class="form-label">
               <span>Employer's name:</span>
            </div>
            <div class="form-control">
                <asp:TextBox runat="server" ID="txtEmployerName" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
            </div>
        </div>

<div class="form-group">
        <div class="form-label">
           <span>Address:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtEmployerAddress" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
        </div>
    </div>



                <div class="form-group">
                    <div class="form-label">
                       <span>Address 2:</span>
                    </div>
                    <div class="form-control">
                        <asp:TextBox runat="server" ID="txtEmpAddress2" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                                    <div class="form-label">
                                       <span>Country:</span>
                                    </div>
                                    <div class="form-control">
                                        <asp:TextBox runat="server" ID="txtEmpCountry" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>

                <div class="form-group">
                    <div class="form-label">
                       <span>Region (Province/State):</span>
                    </div>
                    <div class="form-control">
                        <asp:TextBox runat="server" ID="txtEmpRegion" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-label">
                       <span>City:</span>
                    </div>
                    <div class="form-control">
                        <asp:TextBox runat="server" ID="txtEmpCity" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-label">
                       <span>Postal/Zip Code:</span>
                    </div>
                    <div class="form-control">
                        <asp:TextBox runat="server" ID="txtEmpPostalCode" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>

    </div>
    <!-- /.form-section -->

    <div class="form-group">
        <div class="form-label">
           <span>Occupation:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtOccupation" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label" style="align-self: flex-start">
<span>(if consultant or manager, please elaborate)</span>
        </div>
        <div class="form-control">

            <asp:TextBox runat="server" ID="txtOccupationElaborate" TextMode="MultiLine" Columns="30" Rows="10" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Years of Investment Experience?</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtInvestmentExperienceYear" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Insider on Any Public Companies:</span>
        </div>
        <div class="form-control">
            <asp:RadioButtonList runat="server" ID="radPublicCompanies" RepeatDirection="Horizontal" ClientIDMode="Static">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>                
            </asp:RadioButtonList>
            
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Brokerage Accounts Held Elsewhere?</span>
        </div>
        <div class="form-control">
            <asp:RadioButtonList runat="server" ID="radBrokerageAccounts" RepeatDirection="Horizontal" ClientIDMode="Static">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>                
            </asp:RadioButtonList>
            
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">

        </div>
        <div class="form-control with-label">
        <span>If yes, then where</span>
            <asp:TextBox runat="server" ID="txtBrokerageAccountsPlace" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

<div class="form-group">

        <div class="form-section-title tell-us-about-you">
        <span>2</span>
            <h3>Tell Us About Your Spouse</h3>
        </div>

    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Spouse's Name:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtSpouseName" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Spouse's Employer:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtSpouseEmployer" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Spouse's Occupation:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtSpouseOccupation" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Spouse's Approx Income:</span>
           <div class="label-info">(Indicate Currency)</div>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtSpouseIncome" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

<div class="form-group">

        <div class="form-section-title tell-us-about-you">
        <span>3</span>
            <h3>Your Financial Information </h3> <div class="notes">&nbsp;(Indicate Currency)</div>
        </div>

    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Approx Annual Income:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtAnnualIncome" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Estimated Liquid Assets:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtLiquidAssets" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Estimated Gross Fixed Assets:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtGrossFixedAssets" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

    <div class="form-group">
        <div class="form-label">
           <span>Liabilities:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtLiabilities" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

     <div class="form-group">
        <div class="form-label">
           <span>Estimated Total Net Worth:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtTotalNetWorth" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>

     <div class="form-group">
        <div class="form-label">
           <span>Current Annual Income From All Sources:</span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtAnnualIncomeFromAllSources" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>        
        </div>
    </div>    

    <!-- FORM SECTION -->
    <div class="form-section">

        <span>Bank Information:</span>

            <div class="form-group">
                <div class="form-label">
                   <span>Bank Name:</span>
                </div>
                <div class="form-control">
                    <asp:TextBox runat="server" ID="txtBankName" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                        <div class="form-label">
                           <span>Account Number:</span>
                        </div>
                        <div class="form-control">
                           <asp:TextBox runat="server" ID="txtAccountNumber" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

    <div class="form-group">
                        <div class="form-label">
                           <span>Transit Number:</span>
                        </div>
                        <div class="form-control">
                            <asp:TextBox runat="server" ID="txtTransitNumber" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                                        <div class="form-label" style="align-self: flex-start">
                                           <span>Branch Address:</span>
                                        </div>
                                        <div class="form-control">
                                            <asp:TextBox runat="server" ID="txtBranchAddress" TextMode="MultiLine" Columns="30" Rows="10" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                                                                                        
                                        </div>
                                    </div>

        </div>
        <!-- /.form-section -->

<div class="form-group">

        <div class="form-section-title tell-us-about-you">
        <span>4</span>
            <h3>Your Investment Advisor Information </h3> 
        </div>

    </div>

    <%--<div class="form-group">
        <div class="form-label">
           <span><strong>Select your Investment Advisor</strong></span>
        </div>
        <div class="form-control">
            <asp:TextBox runat="server" ID="txtInvestmentAdvisor" CssClass="form-text" autocomplete="off" ValidationGroup="submit" ClientIDMode="Static" AutoCompleteType="Disabled" placeholder="Type your advisor's name and select from list below"></asp:TextBox>        
            <span id="advisor-error" style="display: none; color:red;">select one of advisors from list or select "Not Found"</span>
        </div>
    </div>--%>
    <div class="form-group">
        <div class="form-label">
           <span><strong>Select your Investment Advisor</strong></span>
        </div>
        <div class="form-control">
            <asp:DropDownList data-placeholder="Type your advisor's name and select from list below" runat="server" ID="ddlAdvisor" class="chzn-select" Style="width: 350px;" ValidationGroup="submit"></asp:DropDownList>
            <asp:RequiredFieldValidator ErrorMessage="&nbsp;&nbsp;* An Investment Advisor has not been selected. This is required" ControlToValidate="ddlAdvisor" InitialValue="" runat="server" ForeColor="Red" ValidationGroup="submit" />
        </div>
        
    </div>
    <div class="form-group">
        <div class="form-label">
           &nbsp;
        </div>
        <div class="form-control">
           <asp:CheckBox runat="server" ID="chkboxConfirm" Text="&nbsp;I confirm that the information provided is accurate." ClientIDMode="Static" ValidationGroup="submit"  />
            
            <asp:CustomValidator  ID="CustomValidator1" ForeColor="Red" runat="server" ClientValidationFunction="ensureChecked" ErrorMessage="* This is required" ValidationGroup="submit"></asp:CustomValidator>
        </div>
    </div>
    

     <div class="form-group">
        <div class="form-label">
             &nbsp;
        </div>
        <div class="form-control">
             <telerik:RadCaptcha ID="RadCaptcha1" runat="server" ErrorMessage="The code you entered is not valid."
                                ValidationGroup="submit" ForeColor="Red">
                                <CaptchaImage ImageCssClass="imageClass" />
                            </telerik:RadCaptcha>
        </div>
    </div>
     <div class="form-group">
        <div class="form-label">
             &nbsp;
        </div>
        <div class="form-control">
            <asp:Button runat="server" ID="btnSubmit" CssClass="clientBtn" ValidationGroup="submit" Text="Submit" OnClick="btnSubmit_Click" />
            <br />
            <asp:Label runat="server" ID="lblMsg" ForeColor="Red" Text="An error happens in the system, please contact your advisor." Visible="false"></asp:Label>
        </div>
    </div>
</div>

<div id="session_time_out">
    <div style="text-align: center;">
        <h3>
            We are sorry.  Your session has expired. 
        </h3>
        <p>
            Please fill in the form again.
        </p>
    </div>
    <div>
        <asp:Button runat="server" ID="Button1" CssClass="clientBtn" Text="OK" OnClick="Button1_Click" />
    </div>
    <div>        
        <p class="disclaimer">
            This web form has been designed to end your session automatically. If the system detects that you have been inactive for 15 minutes, you will be required to complete the web form again. This is for your security and safety. 
        </p>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {        
        
        $("#txtInvestmentAdvisor").autocomplete({
            minLength: 1,
            source: function (request, response) {
                
                $.ajax({
                    url: "/QueryAdvisor.asmx/GetUserIACode",
                    method: "post",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ term: request.term }),
                    dataType: "json",
                    success: function (data) {
                        
                        response(data.d);

                        if (data.d.length > 2) {
                            $("#advisor-error").hide();
                        }
                        else {
                            $("#advisor-error").show();
                        }

                    },
                    error: function (err) {
                        alert("error-msg: " + err);                         
                    }
                });
            }
        });

        //$("#txtInvestmentAdvisor").click(function () {
        //    alert("222");
        //}); 

        /*
        window.onpopstate = function(event) {
            alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };

        if (window.history && window.history.pushState) {
            alert("test11");
            window.history.pushState({page: 1}, "security form", "/security-form");

            $(window).on('popstate', function() {
                alert('Back button was pressed.');
                document.location.href = "/security-form";
            });

        }
        */
    });


    //back button
    /*
    window.onload = function () {
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            // Handle the back (or forward) buttons here
            // Will NOT handle refresh, use onbeforeunload for this.
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
                // Detect and redirect change here
                // Works in older FF and IE9
                // * it does mess with your hash symbol (anchor?) pound sign
                // delimiter on the end of the URL
            }
            else {
                ignoreHashChange = false;   
            }
        };
    }
    }
    */
    //end back button

    //if( window.history && window.history.pushState ){
    //    history.pushState("nohb", null, "");
    //    alert("222");
       
    //    $(window).on( "popstate", function(event){
    //        if( !event.originalEvent.state ){
    //        history.pushState( "nohb", null, "" );
    //        return;
    //        }
    //    });
    //}
    
    console.log('test');
    
    document.getElementById("txtFullLegalName").focus();
    document.getElementById("txtFullLegalName").click();
    //eventFire(document.getElementById("txtFullLegalName"), 'click');


    history.pushState(null, document.title, document.location.href);

    window.onpopstate = function () {
        document.location.href = "/security-form";
        //alert("The back button should not be used 111.");
    };

     //Handle international prefixes, format phone input field
     // Uses intl-tel-input plugin
    const inputCellPhoneField = document.querySelector("#txtCellPhone");
    const inputOfficePhoneField = document.querySelector("#txtOfficePhone");
    const inputHomePhoneField = document.querySelector("#txtHomePhone");

      
    const inputCellPhone = window.intlTelInput(inputCellPhoneField, {        
    preferredCountries: ["ca", "us"],
    utilsScript:
        "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    });

    const inputOfficePhone = window.intlTelInput(inputOfficePhoneField, {        
        preferredCountries: ["ca", "us"],
        utilsScript:
          "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    });

    const inputHomePhone = window.intlTelInput(inputHomePhoneField, {        
        preferredCountries: ["ca", "us"],
        utilsScript:
          "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    });


    function process(event) {
      event.preventDefault();

      const cellPhoneNumber = inputCellPhone.getNumber();      
        
        if (inputCellPhone.isValidNumber()) {
            document.getElementById("hdCellPhoneNumber").value = cellPhoneNumber;
        }
        
        const officePhoneNumber = inputOfficePhone.getNumber();     
        if (inputOfficePhone.isValidNumber()) {
            document.getElementById("hdOfficePhone").value = officePhoneNumber;
        }

        const homePhoneNumber = inputHomePhone.getNumber();     
        if (inputHomePhone.isValidNumber()) {
            document.getElementById("hdHomePhone").value = homePhoneNumber;
        }
        
    }
    
    function eventFire(el, etype) {
        
      if (el.fireEvent) {
        el.fireEvent('on' + etype);
      } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
      }
    }

    function ensureChecked(source, args) { 
        var cb = document.getElementById('chkboxConfirm');

        if ( cb.checked == true ) { 
            args.IsValid = true; 
        } else {
            args.IsValid = false; 
        }
    }


</script>

<%--<script src="/js/jquery.min.js" type="text/javascript"></script>--%>
<script src="/js/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> $(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({ allow_single_deselect: true }); </script>
