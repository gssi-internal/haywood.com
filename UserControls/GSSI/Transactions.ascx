﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Transactions.ascx.cs" Inherits="UserControls_GSSI_Transactions" Debug="true" %>

<div class="tool-transactions">
<div class="sorting">
    <h3>Sort By: </h3>
    <asp:DropDownList ID="drpSorting" runat="server" CssClass="sortby" AutoPostBack="true" OnSelectedIndexChanged="drpSorting_SelectedIndexChanged">
            <asp:ListItem Value="Date">Date</asp:ListItem>
            <asp:ListItem Value="Category">Sector</asp:ListItem>
          
        </asp:DropDownList>
    <asp:Button ID="ascSort" runat="server" OnClick="ascSort_Click"  CssClass="asc-sort"/>
<asp:Button ID="descSort" runat="server" OnClick="descSort_Click" Visible="false"    CssClass="desc-sort"/>
<asp:HiddenField ID="hdSort" Value="Desc" runat="server" />
    <asp:HiddenField ID="hdViewFormat" Value="grid" runat="server" />
    </div>
       <div class="show">
    <h3>Show: </h3>
        <asp:DropDownList ID="changePageItmes" runat="server" AutoPostBack="true" CssClass="showby" OnSelectedIndexChanged="changePageItmes_SelectedIndexChanged">
           
            <asp:ListItem Value="12">12</asp:ListItem>
            <asp:ListItem Value="20">20</asp:ListItem>
        </asp:DropDownList>
</div>
<div class="view">
    <h3>View as: </h3>
        <asp:Button ID="gridView" runat="server" OnClick="gridView_Click"  CssClass="gridView"/>
<asp:Button ID="listView" runat="server" OnClick="listView_Click"    CssClass="listView"/>
</div>
 
    </div>



<asp:Repeater ID="rpGrid" runat="server" EnableViewState="true" Visible="true" >
    <HeaderTemplate>
        <div class="trans-items rpGrid">
            <h1>Recent Transactions - Tombstone View</h1>
            
    </HeaderTemplate>
    <ItemTemplate>
         <%# (Container.ItemIndex + 4) % 4 == 0 ? "<ul class='sfitem sfClearfix' id='transactionsList'>" : string.Empty%>
           <li>
            <div class="items-wrapper">
            <div class="tr-date"><%# Eval("Date") == null ? "" : ((DateTime)Eval("Date")).ToString("MM/dd/yyyy") %></div>
            <div class="tr-type"><%# Eval("Type") %></div>
            <div class="tr-logo">
                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Logo") %>' Width="150" Height="75" /></div>
            <div class="tr-role"><%# Eval("OurRole") %></div>
            <%--<div class="tr-title"><%# Eval("Title") %></div>--%>
            <div class="tr-amount"><%# Eval("Amount") %></div>
            <div class="tr-information"><%# Eval("Information") %></div>
                </div>
            <div class="tr-category"><%# Eval("Category") %></div>
        </li>
       <%# (Container.ItemIndex + 4) % 4 == 3 ? "</ul>" : string.Empty%>
        
    </ItemTemplate>
    <FooterTemplate>
        
        </div>
    </FooterTemplate>
</asp:Repeater>


<asp:Repeater ID="rptlist" runat="server" EnableViewState="true" Visible="false" >
    <HeaderTemplate>
        <div class="trans-items rptlist">
            <h1>Recent Transactions - List View</h1>
            <ul class="sfitem sfClearfix" id="transactionsList">
            <li class="th-header">
                <div class="tr-title">Company</div>
                <div class="tr-role">Our Role</div>
                <div class="tr-information">Ticker</div>
                <div class="tr-amount">Amount</div>
                <div class="tr-date">Date</div>
                <div class="tr-category">Sector</div>
                <div class="tr-type">Type</div>
            </li>
    </HeaderTemplate>

    <ItemTemplate>
        
        <li>
            <!-- <div class="tr-logo">
                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Logo") %>' Width="154" Height="75" />
            </div> -->
            <div class="tr-title"><%# Eval("Title") %></div>
            <div class="tr-role"><%# Eval("OurRole") %></div>
            <div class="tr-information"><%# Eval("Ticker") %></div>
            <div class="tr-amount"><%# Eval("Amount") %></div>
            <div class="tr-date"><%# Eval("Date") == null ? "" : ((DateTime)Eval("Date")).ToString("MM/dd/yyyy") %></div>
            <div class="tr-category"><%# Eval("Category") %></div>
            <div class="tr-type"><%# Eval("Type") %></div>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
        </div>
    </FooterTemplate>
</asp:Repeater>

<asp:Button ID="imgBtnPrev"  runat="server" OnClick="imgBtnPrev_Click" CssClass="page-button"  Text="Previous"/> 


        Page 
        <asp:Literal ID="litPageNum" runat="server" Text="0"></asp:Literal> of 
        <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
        <asp:Button ID="imgBtnNext" 
            runat="server" onclick="imgBtnNext_Click" CssClass="page-button"  Text="Next"/>
