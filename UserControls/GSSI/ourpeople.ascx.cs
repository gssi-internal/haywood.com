﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Text;
using Telerik.Sitefinity.Lists.Model;
using Telerik.Sitefinity.Modules.Lists;
using Telerik.Sitefinity.Workflow;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using Telerik.OpenAccess;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

public partial class UserControls_GSSI_ourpeople : System.Web.UI.UserControl
{

    private string _country = "Canada";

    public string Country
    {
        get { return _country; }
        set { _country = value; }
    }

    public static String filterstring = "";
    public static String currentcategory = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Country.ToLower() == "canada")
            {
                DropDownListall.Visible = true;
                //DropDownListallUS.Visible = false;

                filterstring = Request.QueryString["filter"];
                setbasefilter();

                getfilter();

                ltag1.Visible = ltag2.Visible = ltag3.Visible = ltag4.Visible = ltag5.Visible = true;
                ltag6.Visible = false;
            }
            else //USA
            {
                DropDownListall.Visible = false;
                //DropDownListallUS.Visible = true;

                filterstring = "sales-trading"; //sales-trading, research-analysts
                setbasefilter();

                getfilter();

                ltag1.Visible = ltag2.Visible = ltag3.Visible = ltag4.Visible = ltag5.Visible = false;
                ltag6.Visible = true;
            }
        }


    }


    protected void setbasefilter()
    {
        if (Country.ToLower() == "canada")
        {
            filterstring = Request.QueryString["filter"];

            if (!String.IsNullOrEmpty(filterstring))
            {
                if (filterstring.ToLower().Contains("leadership"))
                {
                    currentcategory = "leadership";
                }
                else if (filterstring.ToLower().Contains("sales-trading"))
                {
                    currentcategory = "sales-trading";
                }
                else if (filterstring.ToLower().Contains("research-analysts"))
                {
                    currentcategory = "research-analysts";
                }
                else if (filterstring.ToLower().Contains("investment-bankers"))
                {
                    currentcategory = "investment-bankers";
                }
            }
            else
                currentcategory = "";
        }
    }

    protected void getfilter()
    {
        isshowfilterbygroup();

        //    Response.Write("<br /> filtersting =" + filterstring + "    <br />");

        List<Guid> filterlist = new List<Guid>();

        if (!String.IsNullOrEmpty(filterstring))
        {
            foreach (string categoryname in filterstring.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
            {
                filterlist.Add(getguidstringbycategoryname(categoryname.ToLower()));
                //     Response.Write("categoryname: " + categoryname + " <br />");
            }
        }

        getourpeoplelistitems(filterlist);
    }

    protected void isshowfilterbygroup()
    {
        if (Country.ToLower() == "canada")
        {
            //Response.Write(currentcategory + "=====xx");
            if (currentcategory == "sales-trading")
            {
                DropDownListgroup.Visible = true;
            }
            else
            {
                DropDownListgroup.Visible = false;
            }
        }
    }

    protected Guid getguidstringbycategoryname(string categoryname)
    {
        TaxonomyManager manager = TaxonomyManager.GetManager();
        HierarchicalTaxonomy categoryvar = manager.GetTaxonomies<HierarchicalTaxonomy>().Where(t => t.Name == "Categories").SingleOrDefault();
        var baseTaxon = manager.GetTaxa<HierarchicalTaxon>().Where(t => t.Title == categoryname).SingleOrDefault();

        //  var baseTaxon = categoryvar.Taxa.Where(t => t.Title == categoryname).SingleOrDefault();

        return baseTaxon.Id;
    }

    protected void getourpeoplelistitems(List<Guid> filterlist)  //filterlist is separated by comma
    {

        List mypeoplelists = GetListByTitleFluentAPI("peoplelists");
        List<Telerik.Sitefinity.Lists.Model.ListItem> iqueryablee = new List<Telerik.Sitefinity.Lists.Model.ListItem>();

        Telerik.Sitefinity.Lists.Model.ListItem mypeoplelistitems = GetListItemByIdFluentAPI(mypeoplelists.Id);

        var listitemspublished = mypeoplelists.ListItems.Where(t => t.Status == ContentLifecycleStatus.Live && t.Visible == true);

        if (Country.ToLower() == "canada")
        {
            listitemspublished = listitemspublished.Where(t => t.GetValue<bool>("ShownInCanada") == true);

            foreach (Telerik.Sitefinity.Lists.Model.ListItem ListItem1 in listitemspublished)
            {
                bool isincollection = true;

                if (filterlist != null && filterlist.Count > 0)  //if there is some filterlist  then  do the filter  else  add all
                {
                    foreach (Guid tmpguidstring in filterlist)
                    {
                        if (!ListItem1.GetValue<TrackedList<Guid>>("Category").Contains((tmpguidstring)))
                        {
                            isincollection = false;
                        }
                    }
                }

                if (isincollection)   //else  will add all
                {
                    iqueryablee.Add(ListItem1);

                }
            }
        }
        else //for USA site only show 'Sales & Trading'
        {
            listitemspublished = listitemspublished.Where(t => t.GetValue<bool>("ShownInUSA") == true);

            foreach (Telerik.Sitefinity.Lists.Model.ListItem ListItem1 in listitemspublished)
            {
                bool isincollection = true;

                if (filterlist != null && filterlist.Count > 0)  //if there is some filterlist  then  do the filter  else  add all
                {
                    foreach (Guid tmpguidstring in filterlist)
                    {
                        if (!ListItem1.GetValue<TrackedList<Guid>>("Category").Contains((tmpguidstring)))
                        {
                            isincollection = false;
                        }
                    }
                }

                if (isincollection)   //else  will add all
                {
                    iqueryablee.Add(ListItem1);

                }
            }
        }

        //  Response.Write("count=" + iqueryablee.Count());

        //  iqueryablee = ( List < Telerik.Sitefinity.Lists.Model.ListItem > )iqueryablee.OrderBy(item => item.Title);

        ListViewourpeople.DataSource = iqueryablee.OrderBy(item => item.Title);
        ListViewourpeople.DataBind();
    }

    public string imageurl()
    {
        string tempstring = Eval("Story").ToString();
        List<Telerik.Sitefinity.Model.IDataItem> listimages = (List<Telerik.Sitefinity.Model.IDataItem>)Eval("Image");
        Telerik.Sitefinity.Libraries.Model.Image relatedimage1 = (Telerik.Sitefinity.Libraries.Model.Image)listimages[0];
        return relatedimage1.Url;


    }


    public Telerik.Sitefinity.Lists.Model.ListItem GetListItemByIdFluentAPI(Guid masterListItemId)
    {
        int count = 0;
        Telerik.Sitefinity.Lists.Model.ListItem listItem = null;

        App.WorkWith().ListItems().Where(i => i.Id == masterListItemId).Count(out count);

        if (count > 0)
        {
            listItem = App.WorkWith().ListItem(masterListItemId).GetLive().Get();
        }

        return listItem;
    }
    public List GetListByTitleFluentAPI(string title)
    {
        List list = App.WorkWith().Lists().Where(l => l.Title == title).Get().FirstOrDefault();
        return list;
    }

    protected void DropDownListall_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Country.ToLower() == "canada")
        {
            ltag1.Visible = ltag2.Visible = ltag3.Visible = ltag4.Visible = ltag5.Visible = true;
            ltag6.Visible = false;
        }
        else //USA
        {
            ltag1.Visible = ltag2.Visible = ltag3.Visible = ltag4.Visible = ltag5.Visible = false;
            ltag6.Visible = true;
        }

        //DropDownListallUS.Visible = false;

        if (currentcategory == "" || currentcategory == "all")
        {
            if (DropDownListall.SelectedValue == "all")
            {
                filterstring = "";



            }
            else if (DropDownListall.SelectedValue == "Vancouver")
            {
                filterstring = "Vancouver";


            }
            else if (DropDownListall.SelectedValue == "Calgary")
            {
                filterstring = "Calgary";

            }
            else if (DropDownListall.SelectedValue == "Toronto")
            {

                filterstring = "Toronto";

            }

        }
        else if (currentcategory == "sales-trading")
        {

            string basefilter = currentcategory;

            if (DropDownListall.SelectedValue == "all")
            {
                filterstring = basefilter;
            }


            else if (DropDownListall.SelectedValue == "Vancouver")
            {
                filterstring = basefilter + "|Vancouver";


            }
            else if (DropDownListall.SelectedValue == "Calgary")
            {
                filterstring = basefilter + "|Calgary";

            }
            else if (DropDownListall.SelectedValue == "Toronto")
            {

                filterstring = basefilter + "|Toronto";

            }


            if (DropDownListgroup.SelectedValue == "all")
            {
                //filterstring += basefilter;
            }


            else if (DropDownListgroup.SelectedValue == "institutional-sales")
            {
                filterstring = filterstring + "|institutional-sales";


            }
            else if (DropDownListgroup.SelectedValue == "retail-sales")
            {
                filterstring = filterstring + "|retail-sales";

            }
            else if (DropDownListgroup.SelectedValue == "portfolio-managers")
            {

                filterstring = filterstring + "|portfolio-managers";

            }




        }
        else if (currentcategory == "leadership")
        {

            string basefilter = currentcategory;

            if (DropDownListall.SelectedValue == "all")
            {
                filterstring = basefilter;
            }


            else if (DropDownListall.SelectedValue == "Vancouver")
            {
                filterstring = basefilter + "|Vancouver";


            }
            else if (DropDownListall.SelectedValue == "Calgary")
            {
                filterstring = basefilter + "|Calgary";

            }
            else if (DropDownListall.SelectedValue == "Toronto")
            {

                filterstring = basefilter + "|Toronto";

            }


        }
        else if (currentcategory == "research-analysts")
        {

            string basefilter = currentcategory;

            if (DropDownListall.SelectedValue == "all")
            {
                filterstring = basefilter;
            }


            else if (DropDownListall.SelectedValue == "Vancouver")
            {
                filterstring = basefilter + "|Vancouver";


            }
            else if (DropDownListall.SelectedValue == "Calgary")
            {
                filterstring = basefilter + "|Calgary";

            }
            else if (DropDownListall.SelectedValue == "Toronto")
            {

                filterstring = basefilter + "|Toronto";

            }


        }
        else if (currentcategory == "investment-bankers")
        {

            string basefilter = currentcategory;

            if (DropDownListall.SelectedValue == "all")
            {
                filterstring = basefilter;
            }


            else if (DropDownListall.SelectedValue == "Vancouver")
            {
                filterstring = basefilter + "|Vancouver";


            }
            else if (DropDownListall.SelectedValue == "Calgary")
            {
                filterstring = basefilter + "|Calgary";

            }
            else if (DropDownListall.SelectedValue == "Toronto")
            {

                filterstring = basefilter + "|Toronto";

            }


        }

        getfilter();

    }

    //protected void DropDownListallUS_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DropDownListall.Visible = false;

    //    if (currentcategory == "" || currentcategory == "all")
    //    {
    //        if (DropDownListallUS.SelectedValue == "all")
    //        {
    //            filterstring = "";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "Los Angeles")
    //        {
    //            filterstring = "Los Angeles";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "New York")
    //        {
    //            filterstring = "New York";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "Seattle")
    //        {
    //            filterstring = "Seattle";
    //        }
    //    }
    //    else if (currentcategory == "sales-trading")
    //    {
    //        string basefilter = currentcategory;

    //        if (DropDownListallUS.SelectedValue == "all")
    //        {
    //            filterstring = basefilter;
    //        }
    //        else if (DropDownListallUS.SelectedValue == "Los Angeles")
    //        {
    //            filterstring = basefilter + "|Los Angeles";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "New York")
    //        {
    //            filterstring = basefilter + "|New York";

    //        }
    //        else if (DropDownListallUS.SelectedValue == "Seattle")
    //        {
    //            filterstring = basefilter + "|Seattle";
    //        }

    //        if (DropDownListgroup.SelectedValue == "all")
    //        {
    //            //filterstring += basefilter;
    //        }
    //        else if (DropDownListgroup.SelectedValue == "institutional-sales")
    //        {
    //            filterstring = filterstring + "|institutional-sales";
    //        }
    //        else if (DropDownListgroup.SelectedValue == "retail-sales")
    //        {
    //            filterstring = filterstring + "|retail-sales";
    //        }
    //        else if (DropDownListgroup.SelectedValue == "portfolio-managers")
    //        {
    //            filterstring = filterstring + "|portfolio-managers";
    //        }
    //    }
    //    else if (currentcategory == "leadership")
    //    {
    //        string basefilter = currentcategory;

    //        if (DropDownListallUS.SelectedValue == "all")
    //        {
    //            filterstring = basefilter;
    //        }
    //        else if (DropDownListallUS.SelectedValue == "Los Angeles")
    //        {
    //            filterstring = basefilter + "|Los Angeles";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "New York")
    //        {
    //            filterstring = basefilter + "|New York";

    //        }
    //        else if (DropDownListallUS.SelectedValue == "Seattle")
    //        {
    //            filterstring = basefilter + "|Seattle";
    //        }
    //    }

    //    else if (currentcategory == "research-analysts")
    //    {

    //        string basefilter = currentcategory;

    //        if (DropDownListallUS.SelectedValue == "all")
    //        {
    //            filterstring = basefilter;
    //        }
    //        else if (DropDownListallUS.SelectedValue == "Los Angeles")
    //        {
    //            filterstring = basefilter + "|Los Angeles";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "New York")
    //        {
    //            filterstring = basefilter + "|New York";

    //        }
    //        else if (DropDownListallUS.SelectedValue == "Seattle")
    //        {
    //            filterstring = basefilter + "|Seattle";
    //        }
    //    }
    //    else if (currentcategory == "investment-bankers")
    //    {

    //        string basefilter = currentcategory;

    //        if (DropDownListallUS.SelectedValue == "all")
    //        {
    //            filterstring = basefilter;
    //        }
    //        else if (DropDownListallUS.SelectedValue == "Los Angeles")
    //        {
    //            filterstring = basefilter + "|Los Angeles";
    //        }
    //        else if (DropDownListallUS.SelectedValue == "New York")
    //        {
    //            filterstring = basefilter + "|New York";

    //        }
    //        else if (DropDownListallUS.SelectedValue == "Seattle")
    //        {
    //            filterstring = basefilter + "|Seattle";
    //        }
    //    }

    //    getfilter();

    //}

    protected void DropDownListgroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Country.ToLower() == "canada")
        {
            ltag1.Visible = ltag2.Visible = ltag3.Visible = ltag4.Visible = ltag5.Visible = true;
            ltag6.Visible = false;
        }
        else //USA
        {
            ltag1.Visible = ltag2.Visible = ltag3.Visible = ltag4.Visible = ltag5.Visible = false;
            ltag6.Visible = true;
        }

        string basefilter = "sales-trading";
        if (DropDownListall.SelectedValue == "all")
        {
            filterstring = basefilter;
        }
        else if (DropDownListall.SelectedValue == "Vancouver")
        {
            filterstring = basefilter + "|Vancouver";
        }
        else if (DropDownListall.SelectedValue == "Calgary")
        {
            filterstring = basefilter + "|Calgary";
        }
        else if (DropDownListall.SelectedValue == "Toronto")
        {
            filterstring = basefilter + "|Toronto";
        }

        if (DropDownListgroup.SelectedValue == "all")
        {
            //filterstring += basefilter;
        }
        else if (DropDownListgroup.SelectedValue == "institutional-sales")
        {
            filterstring = filterstring + "|institutional-sales";
        }
        else if (DropDownListgroup.SelectedValue == "retail-sales")
        {
            filterstring = filterstring + "|retail-sales";
        }
        else if (DropDownListgroup.SelectedValue == "portfolio-managers")
        {
            filterstring = filterstring + "|portfolio-managers";
        }
        getfilter();
    }
}