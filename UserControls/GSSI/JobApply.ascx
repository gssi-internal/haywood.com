﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JobApply.ascx.cs" Inherits="UserControls_GSSI_JobApply" %>
<%@ Register Src="~/scripts/formmail.ascx" TagPrefix="GSSI" TagName="FormMail" %>

<GSSI:FormMail ID="FormMail1" runat="server" 
			TemplateID="formtemplate"
			FromAddress="sitefinity@graphicallyspeaking.ca"
	
            Recipients="allan@graphicallyspeaking.ca "

            Subject="Haywood Job Apply Submission"
			SubmitButtonID="ButtonSubmit"
			OnSubmit="FormMail_Submitted"
       
            OnSendingEmail="FormMail_SendingEmail"
		/>

	


<asp:Label ID="checkHandler" runat="server"></asp:Label><br /><br />
 
<asp:literal ID="emailoutput" runat="server" />



<div class="jobapplycontent">


    <table id="formtemplate" border="0"    class="contacttb" runat="server" >
    <tr>
        <td>

            <span>
                Job Title:
            </span>
        </td>
        <td>
            <asp:TextBox ID="TextBoxjobtitle" Class="txtbox"  Enabled="false" runat="server"></asp:TextBox>

        </td>



    </tr>



    <tr>
        <td>

           <span>Name: </span>  
        
        </td>

        <td>
            <asp:TextBox ID="TextBoxName" Class="txtbox"  runat="server"></asp:TextBox>
        </td>

    </tr>


        <tr>
           <td>
               <span>Address:</span>
           </td>

            <td>
                <asp:TextBox ID="TextBoxAddress" Class="txtbox"  runat="server"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td>
                <span>
                    City:
                </span>

            </td>
            <td>
                 <asp:TextBox ID="TextBoxCity" Class="txtbox"  runat="server"></asp:TextBox>
            </td>


        </tr>

        <tr>
            <td>
                <span>
                    Province:
                </span>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProvince" Class="txtbox"  runat="server"></asp:TextBox>
            </td>

        </tr>


         <tr>
            <td>
                <span>
                    Email Address:
                </span>
            </td>
            <td>
                 <asp:TextBox ID="TextBoxEmail" Class="txtbox"  runat="server"></asp:TextBox>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"  ForeColor="Red"
         ControlToValidate="TextBoxEmail" ErrorMessage="Not a valid Email! " 
         ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>



            </td>

        </tr>


         <tr>
            <td>
                <span>
                    Phone Number:
                </span>
            </td>
            <td>
                   <asp:TextBox ID="TextBoxPhone" Class="txtbox"  runat="server"></asp:TextBox>
            </td>

        </tr>


        <tr>
            <td>
                <span>
                    Comments:
                </span>
            </td>
            <td>
                   <asp:TextBox ID="TextBoxComments" Class="txtbox"  runat="server"></asp:TextBox>
            </td>

        </tr>

       

        <tr>
            <td>
                <span>
                    Resume:
                </span>
            </td>
            <td>
                  
                   <asp:FileUpload ID="FileUploadResume" runat="server" />

            </td>

        </tr>

         <tr>
            <td>
                <span>
                    Cover Letter:
                </span>
            </td>
            <td>
                  
                   <asp:FileUpload ID="FileUploadCover" runat="server" />

            </td>

        </tr>



        <tr>

            <td>
                <span> </span>

            </td>

            <td>
                
                <asp:Button ID="ButtonSubmit"   Class="btncontactsubmit "  runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />


            </td>

        </tr>


    
    </table>

 



</div>
