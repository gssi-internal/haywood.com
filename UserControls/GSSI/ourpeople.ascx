﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ourpeople.ascx.cs" Inherits="UserControls_GSSI_ourpeople" %>

<link href="App_Themes/ourpeoplenw/Global/ourpeoplenw.css?v=3" type="text/css" rel="stylesheet" />
<div id="people-filter">
    <ul class="button-group" data-filter-group="main">
        <li runat="server" id="ltag1"><a class="all" href="<%=HttpContext.Current.Request.Url.AbsolutePath %>">All</a>


        </li>
        <li runat="server" id="ltag2"><a class="leadership" href='<%=HttpContext.Current.Request.Url.AbsolutePath.ToString()+"?filter=Leadership" %>'>Board of Directors</a>

            <%--   <asp:DropDownList ID="DropDownListLeadership" AutoPostBack="true" OnSelectedIndexChanged="DropDownListLeadership_SelectedIndexChanged"  runat="server">
                                       <asp:ListItem Value="all">Filter All By Branch</asp:ListItem>
                                       <asp:ListItem>Vancouver</asp:ListItem>
                                       <asp:ListItem>Calgary</asp:ListItem>
                                       <asp:ListItem>Toronto</asp:ListItem>
                                </asp:DropDownList>--%>

        </li>
        <li runat="server" id="ltag3"><a class="sales-trading" href='<%=HttpContext.Current.Request.Url.AbsolutePath.ToString()+"?filter=Sales-Trading" %>'>Sales & Trading</a>
            <%--       <asp:DropDownList ID="DropDownListSalesTrading" AutoPostBack="true" OnSelectedIndexChanged="DropDownListSalesTrading_SelectedIndexChanged"  runat="server">
                                       <asp:ListItem   Value="all">Filter All By Branch</asp:ListItem>
                                       <asp:ListItem>Vancouver</asp:ListItem>
                                       <asp:ListItem>Calgary</asp:ListItem>
                                       <asp:ListItem>Toronto</asp:ListItem>
                                </asp:DropDownList>--%>


                           

        </li>
        <li runat="server" id="ltag4"><a class="research-analysts" href='<%=HttpContext.Current.Request.Url.AbsolutePath.ToString()+"?filter=research-analysts" %>'>Research Analysts</a>
            <%--      <asp:DropDownList ID="DropDownListResearchanalysts" AutoPostBack="true" OnSelectedIndexChanged="DropDownListResearchanalysts_SelectedIndexChanged"  runat="server">
                                       <asp:ListItem Value="all">Filter All By Branch</asp:ListItem>
                                       <asp:ListItem>Vancouver</asp:ListItem>
                                       <asp:ListItem>Calgary</asp:ListItem>
                                       <asp:ListItem>Toronto</asp:ListItem>
                                </asp:DropDownList>--%>
        </li>
        <li runat="server" id="ltag5"><a class="investment-bankers" href='<%=HttpContext.Current.Request.Url.AbsolutePath.ToString()+"?filter=investment-bankers" %>'>Investment Bankers</a>
            <%--   <asp:DropDownList ID="DropDownListInvestmentbankers" AutoPostBack="true" OnSelectedIndexChanged="DropDownListInvestmentbankers_SelectedIndexChanged"  runat="server">
                                       <asp:ListItem Value="all">Filter All By Branch</asp:ListItem>
                                       <asp:ListItem>Vancouver</asp:ListItem>
                                       <asp:ListItem>Calgary</asp:ListItem>
                                       <asp:ListItem>Toronto</asp:ListItem>
                                </asp:DropDownList>--%>
        </li>
        <li runat="server" id="ltag6" class="usa-tag"><a class="all">Our People</a> </li>
    </ul>
    <asp:DropDownList ID="DropDownListall" OnSelectedIndexChanged="DropDownListall_SelectedIndexChanged" AutoPostBack="true" runat="server">
        <asp:ListItem Enabled="false">Please Choose Branch</asp:ListItem>
        <asp:ListItem Value="all">Filter All By Branch</asp:ListItem>
        <asp:ListItem>Vancouver</asp:ListItem>
        <asp:ListItem>Calgary</asp:ListItem>
        <asp:ListItem>Toronto</asp:ListItem>
    </asp:DropDownList>
    <%--<asp:DropDownList ID="DropDownListallUS" OnSelectedIndexChanged="DropDownListallUS_SelectedIndexChanged" AutoPostBack="true" runat="server">
        <asp:ListItem Enabled="false">Please Choose Branch</asp:ListItem>
        <asp:ListItem Value="all">Filter All By Branch</asp:ListItem>
        <asp:ListItem>Los Angeles</asp:ListItem>
        <asp:ListItem>New York</asp:ListItem>
        <asp:ListItem>Seattle</asp:ListItem>
    </asp:DropDownList>--%>
    <asp:DropDownList ID="DropDownListgroup" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="DropDownListgroup_SelectedIndexChanged" runat="server">
        <asp:ListItem Enabled="false">Please Choose Group</asp:ListItem>
        <asp:ListItem Value="all">Filter All By Group</asp:ListItem>
        <asp:ListItem Value="institutional-sales">Institutional Sales</asp:ListItem>
        <asp:ListItem Value="retail-sales">Retail Sales</asp:ListItem>
        <asp:ListItem Value="portfolio-managers">Portfolio Managers</asp:ListItem>
    </asp:DropDownList>

    <%--    <ul class="button-group" data-filter-group="branch">
                      <li><a class="allbranch" href="#" >Filter All By Branch</a></li>
                      <!-- <li><a data-filter="" href="#" class="on">All</a></li> -->
                      <li><a class=".vancouver" href="#">Vancouver</a></li>
                      <li><a class=".calgary" href="#">Calgary</a></li>
                      <li><a class=".toronto" href="#">Toronto</a></li>
                    </ul>
                    <ul class="button-group" data-filter-group="s-t-group">
                      <li><a class="allgroup" href="#" class="on">Filter All By Group</a></li>
                      <!-- <li><a data-filter="" href="#" class="on">All</a></li> -->
                      <li><a class=".institutional-sales" href="#">Institutional Sales</a></li>
                      <li><a class=".retail-sales" href="#">Retail Sales</a></li>
                      <li><a class=".portfolio-managers" href="#">Portfolio Managers</a></li>
                    </ul>--%>
</div>

<div id="people-viewer">
    <div class="viewer-wrapper">
        <span class="close">close</span>
        <div class="content"></div>
    </div>
</div>

<div>

    <asp:Literal ID="Literal1" runat="server"></asp:Literal>


</div>

<div id="people-list">
    <asp:ListView ID="ListViewourpeople" runat="server">

        <LayoutTemplate>
            <ul>
                <asp:PlaceHolder ID="itemPlaceholder" runat="server" />

            </ul>

        </LayoutTemplate>

        <ItemTemplate>
            <li class="item">
                <div class="init-wrapper">
                    <div class="thumb">
                        <img alt="Image" src="<%#imageurl()%>" />
                    </div>
                    <div class="info">
                        <h3>
                            <asp:Label runat="server" ID="Label2"><%#Eval("Title") %></asp:Label></h3>
                        <%--<em>
                            <asp:Label runat="server" ID="Label3"><%#Eval("Position") %></asp:Label></em>--%>
                    </div>
                </div>

                <div class="info-wrapper">
                    <div class="thumb">
                        <img alt="Image" src="<%#imageurl()%>" />
                    </div>
                    <div class="info">
                        <h3>
                            <asp:Label runat="server" ID="Label1"><%#Eval("Title") %></asp:Label></h3>
                        <em>
                            <asp:Label runat="server" ID="lblName" Visible='<%# this.Country.ToLower() == "canada" %>'><%#Eval("Position") %></asp:Label>
                            <asp:Label runat="server" ID="lblNameUSA" Visible='<%# this.Country.ToLower() == "usa" %>'><%#Eval("PositionUSA") %></asp:Label>
                        </em>
                    </div>
                    <div class="story">
                        <asp:Label runat="server" ID="lblType" Visible='<%# this.Country.ToLower() == "canada" %>'> <%#Eval("Story") %> </asp:Label>
                        <asp:Label runat="server" ID="lblTypeUSA" Visible='<%# this.Country.ToLower() == "usa" %>'> <%#Eval("StoryUSA") %> </asp:Label>
                    </div>
                    <span class="close">close</span>

                </div>
            </li>

        </ItemTemplate>

    </asp:ListView>

</div>


<%--<script type="text/javascript" src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/js/jquery.ba-bbq.min.js"></script>
<script type="text/javascript" src="/js/jquery.waypoints.min.js"></script>--%>
<script src="/js/fancybox/jquery.fancybox.js"></script>


<script>
    $(function () {

        var peopleListContainer = $('#people-list'),
            peopleListItem = $('.item', peopleListContainer)
        infoWrapper = $('.info-wrapper', peopleListItem),
            closeButton = $('.close', infoWrapper),
            overlay = $('#overlay');
        wrapper = $('#wrapper');

        peopleListItem.on('click', function (e) {
            $('.pop-up-wrap').html('<div class="info-wrapper">' + $(this).find(infoWrapper).html() + '</div>');
            peopleListItem.removeClass('large');
            overlay.css({
                'visibility': 'Visible',
                'opacity': 1,
            })
            wrapper.css({
                'filter': 'blur(6px)'
            })
            $('.pop-up-wrap').addClass('large');

            // if($('html').hasClass('no-touch')){
            //   $('html,body').css({
            //     'height':'100vh',
            //     'overflow':'hidden',
            //   })
            // }else{
            $('html,body').css({
                'overflow': 'hidden',
                'position': 'relative',
            })
            // }

            $('html,body').on('touchmove', function (e) {
                if (!$('.info-wrapper').has($(e.target)).length) {
                    e.preventDefault();
                }
                // e.preventDefault(); 
                // e.stopPropagation(); 
                // $().unbind('touchmove');
            });

            close();

            //   e.stopPropagation();
        })

        function close() {
            var closeButtonPop = $('.close');
            closeButtonPop.on('click', function (e) {
                $('.pop-up-wrap').html('');
                $('.pop-up-wrap').removeClass('large');
                overlay.css({
                    'visibility': 'hidden',
                    'opacity': 0,
                })
                wrapper.css({
                    'filter': 'none'
                })
                $('html,body').css({
                    'overflow': 'auto',
                    'position': 'static',
                    'height': 'auto',
                })

                $('html,body').unbind('touchmove');
                e.stopPropagation();
            })

            overlay.on('click', function () {
                closeButton.click();
            })
        }

    })

  // $(document).ready(function(){

  //   $(".item").on('click', function() {
  //     $.fancybox.open({
  //       src  : '#pop-up',
  //       type : 'inline',
  //       opts : {
  //         afterShow : function( instance, current ) {
  //           console.info('done!');
  //         }
  //       }
  //     });
  //   });
  // });
</script>
<style>
    #people-filter .usa-tag a.on:after, #people-filter.usa-tag a:hover:after{
        content: '';
    }

    #people-filter ul li.usa-tag {
        text-align:left;
    }
</style>
