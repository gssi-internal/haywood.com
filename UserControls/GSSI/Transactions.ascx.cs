﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Model.ContentLinks;
using System.Text;
using Telerik.OpenAccess;
using System.Data;
using Telerik.Sitefinity.Data.ContentLinks;
using Telerik.Sitefinity.RelatedData;


public partial class UserControls_GSSI_Transactions : System.Web.UI.UserControl
{
    protected int pageSize = 10;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            LoadAllTransactions(0);
        }

    }

    protected void LoadAllTransactions(int pageIndex)
    {
        var providerName = String.Empty;
        DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
        Type transactionsType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Transactions.Transactions");
        var myCollection = dynamicModuleManager.GetDataItems(transactionsType).Where(p => p.Visible == true);
        TrackedList<Guid> cats = new TrackedList<Guid>();
        DataTable tempTable = new DataTable("Products");
        tempTable.Columns.Add("Title", typeof(string));
        tempTable.Columns.Add("Type", typeof(string));
        tempTable.Columns.Add("Amount", typeof(string));
        tempTable.Columns.Add("Information", typeof(string));
        tempTable.Columns.Add("Date", typeof(DateTime));
        tempTable.Columns.Add("Logo", typeof(string));
        tempTable.Columns.Add("Category", typeof(string));
        tempTable.Columns.Add("Ticker", typeof(string));
        tempTable.Columns.Add("OurRole", typeof(string));
        StringBuilder sb = new StringBuilder("");
        TaxonomyManager tManager = TaxonomyManager.GetManager();
        providerName = String.Empty;
        var contentLinksManager = ContentLinksManager.GetManager();
        var libraryManger = LibrariesManager.GetManager();
        dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
        Type testType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Transactions.Transactions");


        foreach (DynamicContent item in myCollection)
        {
            Telerik.Sitefinity.Libraries.Model.Image imageField = item.GetRelatedItems<Telerik.Sitefinity.Libraries.Model.Image>("Logo").FirstOrDefault();
            //var tt = item.GetRelatedItems<Telerik.Sitefinity.Libraries.Model.Image>("Logo");
            //Response.Write(tt.Count());

            //var relativeUrl = imageField.ResolveMediaUrl();
            //var absolute = imageField.Url;
            //var thumbnailUrl = imageField.ThumbnailUrl;

            DataRow nRow = tempTable.NewRow();


            nRow["Title"] = item.GetValue("Title");

            nRow["Type"] = item.GetValue("Type");
            nRow["Amount"] = item.GetValue("Amount");
            nRow["Information"] = item.GetValue("Information");
            nRow["Date"] = (DateTime)item.GetValue("Date");

            if (imageField != null)
                nRow["Logo"] = imageField.ThumbnailUrl;
            else
                nRow["Logo"] = "";

            cats = item.GetValue<TrackedList<Guid>>("Category");
            sb = new StringBuilder("");


            if (cats != null && cats.Count > 0)
            {
                foreach (var catItem in cats)
                {

                    var catName = tManager.GetTaxon<HierarchicalTaxon>(catItem).Title;
                    sb.Append(catName.ToString());
                }

            }

            nRow["Category"] = sb.ToString();
            nRow["Ticker"] = item.GetValue("Ticker");
            nRow["OurRole"] = item.GetValue("OurRole");
            tempTable.Rows.Add(nRow);
        }

        if (drpSorting.SelectedValue != "")
        {
            DataView dv = tempTable.DefaultView;
            dv.Sort = drpSorting.SelectedValue + " " + hdSort.Value;
            tempTable = dv.ToTable();
        }

        pageSize = int.Parse(changePageItmes.SelectedValue);

        PagedDataSource objPage = new PagedDataSource();
        objPage.DataSource = tempTable.DefaultView;
        objPage.AllowPaging = true;
        objPage.PageSize = pageSize;
        if (pageIndex >= objPage.PageCount)
            pageIndex = objPage.PageCount - 1;
        objPage.CurrentPageIndex = pageIndex;

        litPageNum.Text = ((int)(pageIndex + 1)).ToString();
        litTotalPages.Text = objPage.PageCount.ToString();
        imgBtnPrev.Visible = pageIndex > 0;
        imgBtnNext.Visible = pageIndex < (objPage.PageCount - 1);

        if (hdViewFormat.Value == "list")
        {
            rptlist.DataSource = objPage;
            rptlist.DataBind();
        }
        else
        {
            rpGrid.DataSource = objPage;
            rpGrid.DataBind();
        }

    }



    protected void imgBtnPrev_Click(object sender, EventArgs e)
    {
        int pageIndex = int.Parse(litPageNum.Text) - 2;
        if (pageIndex < 0)
            pageIndex = 0;
            
        LoadAllTransactions(pageIndex);
    }

    protected void imgBtnNext_Click(object sender, EventArgs e)
    {
        int pageIndex = int.Parse(litPageNum.Text);
        if (pageIndex < 0)
            pageIndex = 0;
       
        LoadAllTransactions(pageIndex);
    }


    protected void ascSort_Click(object sender, EventArgs e)
    {
        int pageIndex = int.Parse(litPageNum.Text) - 2;
        if (pageIndex < 0)
            pageIndex = 0;
        hdSort.Value = " ASC";
        LoadAllTransactions(pageIndex);

        ascSort.Visible = false;
        descSort.Visible = true;
    }
    protected void descSort_Click(object sender, EventArgs e)
    {
        int pageIndex = int.Parse(litPageNum.Text) - 2;
        if (pageIndex < 0)
            pageIndex = 0;
        hdSort.Value = " DESC";
        LoadAllTransactions(pageIndex);
        
        ascSort.Visible = true;
        descSort.Visible = false;
    }

    protected void gridView_Click(object sender, EventArgs e)
    {

        hdViewFormat.Value = "grid";               
        LoadAllTransactions(0);

       
        rpGrid.Visible = true;
        rptlist.Visible = false;
    }
    protected void listView_Click(object sender, EventArgs e)
    {

        hdViewFormat.Value = "list";             
        LoadAllTransactions(0);
        

        rpGrid.Visible = false;
        rptlist.Visible = true;
    }

    protected void drpSorting_SelectedIndexChanged(object sender, EventArgs e)
    {

        int pageIndex = int.Parse(litPageNum.Text) - 2;
        if (pageIndex < 0)
            pageIndex = 0;        
            
        LoadAllTransactions(pageIndex);
    }

    protected void changePageItmes_SelectedIndexChanged(object sender, EventArgs e)
    {
        pageSize = int.Parse(changePageItmes.SelectedValue);

        int pageIndex = 0;           
            
        LoadAllTransactions(pageIndex);

    }

}