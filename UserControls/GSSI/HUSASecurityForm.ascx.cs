﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Forms;
using Telerik.Sitefinity.Forms.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Abstractions;
using Telerik.Web.UI;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.OpenAccess;
using Telerik.Sitefinity.DynamicModules.Model;
using System.ComponentModel;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Security.Claims;
using System.Globalization;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class UserControls_GSSI_HUSASecurityForm : System.Web.UI.UserControl
{

    #region Properties
    private string _thankYouPage = "/husa-security-form/thank-you";
    [DisplayName("Thank You Page Link")]
    public string ThankYouPage
    {
        get { return _thankYouPage; }
        set { _thankYouPage = value; }
    }

    private string _defaultEmail = "jesse@graphicallyspeaking.ca";
    [DisplayName("Default Email address")]
    public string DefaultEmail
    {
        get { return _defaultEmail; }
        set { _defaultEmail = value; }
    }

    private string _emailSubject = "HUSA Security Form";
    [DisplayName("Email Subject")]
    public string EmailSubject
    {
        get { return _emailSubject; }
        set { _emailSubject = value; }
    }

    private string _fromEmail = "jesse@graphicallyspeaking.ca";
    [DisplayName("From Email address")]
    public string FromEmailAddress
    {
        get { return _fromEmail; }
        set { _fromEmail = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        RadCaptcha1.Display = ValidatorDisplay.Dynamic;
        if (!Page.IsPostBack)
        {
            Page.Form.Attributes.Add("autocomplete", "off");

            ddlAdvisor.DataSource = GetSecurityAdvisors();
            ddlAdvisor.DataBind();
        }

        Page.MaintainScrollPositionOnPostBack = true;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && RadCaptcha1.IsValid)
        {
            //btnSubmit.Attributes.Add("onclick", " this.disabled = true; " + Page.ClientScript.GetPostBackEventReference(btnSubmit, null) + ";");

            try
            {
                btnSubmit.Enabled = false;

                //get an instance of the FormsManager
                FormsManager formsManager = FormsManager.GetManager();

                //retrieve the form by name
                var form = formsManager.GetFormByName("sf_husapublicsecurityform");

                if (form != null)
                {
                    var entryTypeName = String.Format("{0}.{1}", formsManager.Provider.FormsNamespace, form.Name);
                    FormEntry entry = formsManager.CreateFormEntry(entryTypeName);

                    //set value to the text box control with title FirstName

                    entry.SetValue("FormTextBox_FullLegalName", txtFullLegalName.Text.Trim());
                    entry.SetValue("FormTextBox_MaritalStatus", radListMatrialStatus.SelectedValue);
                    entry.SetValue("FormTextBox_NumberOfDependents", txtNumberOfDependents.Text.Trim());
                     
                    entry.SetValue("FormTextBox_YearsAtResidence", txtYearsAtResidence.Text.Trim());

                    entry.SetValue("FormTextBox_Address", txtAddress.Text.Trim());

                    entry.SetValue("FormTextBox_Address2", txtAddress2.Text.Trim());

                    entry.SetValue("FormTextBox_City", txtCity.Text.Trim());

                    entry.SetValue("FormTextBox_Region", txtRegion.Text.Trim());

                    entry.SetValue("FormTextBox_PostalCode", txtPostalCode.Text.Trim());

                    entry.SetValue("FormTextBox_Country", txtCountry.Text.Trim());

                    //entry.SetValue("FormTextBox_RentOrOwn", radRentOwn.SelectedValue);

                    entry.SetValue("FormTextBox_CellPhone", hdCellPhoneNumber.Value);
                    entry.SetValue("FormTextBox_OfficePhone", hdOfficePhone.Value);
                    entry.SetValue("FormTextBox_HomePhone", hdHomePhone.Value);
                    entry.SetValue("FormTextBox_DateBirth", txtDateOfBirthMonth.Text.Trim() + "/" + txtDateOfBirthDay.Text.Trim() + "/" + txtDateOfBirthYear.Text.Trim());
                    entry.SetValue("FormTextBox_TaxIDNumber", txtTaxID.Text.Trim());
                    
                    entry.SetValue("FormTextBox_Citizenship", txtCitizenship.Text.Trim());
                    entry.SetValue("FormTextBox_AlienStatus", txtAlienStatus.Text.Trim());                    

                    entry.SetValue("FormTextBox_EmployerName", txtEmployerName.Text.Trim());
                    entry.SetValue("FormTextBox_EmployerAddress", txtEmployerAddress.Text.Trim());
                    entry.SetValue("FormTextBox_EmpAddress2", txtEmpAddress2.Text.Trim());
                    entry.SetValue("FormTextBox_EmpCity", txtEmpCity.Text.Trim());
                    entry.SetValue("FormTextBox_EmpRegion", txtEmpRegion.Text.Trim());
                    entry.SetValue("FormTextBox_EmpPostalZipCode", txtEmpPostalCode.Text.Trim());
                    entry.SetValue("FormTextBox_EmpCountry", txtEmpCountry.Text.Trim());
                    entry.SetValue("FormTextBox_EmployerPhone", txtEmployerPhone.Text.Trim());
                   

                    entry.SetValue("FormTextBox_Occupation", txtOccupation.Text.Trim());
                    entry.SetValue("FormParagraphTextBox_OccupationElaborate", txtOccupationElaborate.Text.Trim());
                    entry.SetValue("FormTextBox_InvestmentExperienceYear", txtInvestmentExperienceYear.Text.Trim());
                    entry.SetValue("FormTextBox_PublicCompanies", radPublicCompanies.SelectedValue);
                    //entry.SetValue("FormTextBox_BrokerageAccounts", radBrokerageAccounts.SelectedValue);
                    //entry.SetValue("FormTextBox_BrokerageAccountsPlaces", txtBrokerageAccountsPlace.Text.Trim());
                    //entry.SetValue("FormTextBox_SpouseName", txtSpouseName.Text.Trim());
                    //entry.SetValue("FormTextBox_SpouseEmployer", txtSpouseEmployer.Text.Trim());
                    //entry.SetValue("FormTextBox_SpouseOccupation", txtSpouseOccupation.Text.Trim());
                    //entry.SetValue("FormTextBox_SpouseIncome", txtSpouseIncome.Text.Trim());
                    entry.SetValue("FormTextBox_InstitutionName", txtInstitutionName.Text.Trim());

                    //entry.SetValue("FormTextBox_LiquidAssets", txtLiquidAssets.Text.Trim());
                    //entry.SetValue("FormTextBox_GrossFixedAssets", txtGrossFixedAssets.Text.Trim());
                    //entry.SetValue("FormTextBox_Liabilities", txtLiabilities.Text.Trim());

                    //entry.SetValue("FormTextBox_TotalNetWorth", txtTotalNetWorth.Text.Trim());

                    //entry.SetValue("FormTextBox_AnnualIncomeFromAllSources", txtAnnualIncomeFromAllSources.Text.Trim());

                    entry.SetValue("FormTextBox_InvestmentAdvisor", ddlAdvisor.SelectedItem.Text);
                    // txtInvestmentAdvisor.Text.Trim()


                    //entry.SetValue("FormTextBox_BankName", txtBankName.Text.Trim());

                    //entry.SetValue("FormTextBox_AccountNumber", txtAccountNumber.Text.Trim());

                    //entry.SetValue("FormTextBox_TransitNumber", txtTransitNumber.Text.Trim());

                    //entry.SetValue("FormParagraphTextBox_BranchAddress", txtBranchAddress.Text.Trim());

                    //set the response properties as UserId, IPAddress, the datе when the response is submitted 
                    entry.IpAddress = SystemManager.CurrentHttpContext.Request.UserHostAddress;
                    entry.SubmittedOn = DateTime.UtcNow;
                    entry.UserId = ClaimsManager.GetCurrentUserId();

                    //set the response language if multisite environment
                    if (SystemManager.CurrentContext.AppSettings.Multilingual)
                    {
                        entry.Language = CultureInfo.CurrentUICulture.Name;
                    }

                    //set the referral code
                    entry.ReferralCode = formsManager.Provider.GetNextReferralCode(entryTypeName).ToString();

                    //always call save changes to preserve to database
                    formsManager.SaveChanges();

                    string advisorEmail = GetSecurityAdvisorEmail(ddlAdvisor.SelectedItem.Text);
                    //txtInvestmentAdvisor.Text.Trim()
                    if (advisorEmail == "")
                        advisorEmail = DefaultEmail;
                    SendEmailNotification(advisorEmail);

                    Response.Redirect(ThankYouPage, false);
                }

                
            }
            catch (Exception ex)
            {
                WriteLog("HUSSASecurityForm", "Submit form error: \r\n" + ex.Message + "\r\n" + ex.StackTrace);
            }


        }        
    }

    protected void SendEmailNotificationToClient(string emailAddress)
    {
        try
        {
            string logoImg = GetSiteRoot() + "/img/haywood-logo.png";

            FileInfo template1 = new FileInfo(Server.MapPath("~/UserControls/EmailTemplates/HUSA_ClientEmailNotification.html")); ;
            StreamReader sr1 = template1.OpenText();
            string templateContent = sr1.ReadToEnd();
            sr1.Close();

            templateContent = templateContent.Replace("**LOGOIMGID**", logoImg);
            templateContent = templateContent.Replace("**YEAR**", DateTime.Now.Year.ToString());


            //templateContent = templateContent.Replace("**InvestmentAdvisor**", txtInvestmentAdvisor.Text);


            MailingProvider mp = new MailingProvider();
            mp.Subject = EmailSubject;
            mp.ToAddr = emailAddress;
            mp.Body = templateContent;
            mp.FromAddr = FromEmailAddress;
            mp.SendMail();

        }
        catch (Exception ex)
        {
            WriteLog("HUSASecurityForm", "Email notification error: \r\n" + ex.Message + "\r\n" + ex.StackTrace);
        }
    }


    protected void SendEmailNotification(string emailAddress)
    {
        try
        {
            string logoImg = GetSiteRoot() + "/img/haywood-logo.png";

            FileInfo template1 = new FileInfo(Server.MapPath("~/UserControls/EmailTemplates/HUSA_SecurityFormNotification.html")); ;
            StreamReader sr1 = template1.OpenText();
            string templateContent = sr1.ReadToEnd();
            sr1.Close();

            templateContent = templateContent.Replace("**LOGOIMGID**", logoImg);
            templateContent = templateContent.Replace("**YEAR**", DateTime.Now.Year.ToString());

            templateContent = templateContent.Replace("**FullLegalName**", txtFullLegalName.Text);
            templateContent = templateContent.Replace("**MatrialStatus**", radListMatrialStatus.SelectedValue);
            templateContent = templateContent.Replace("**NumberOfDependents**", txtNumberOfDependents.Text);
            templateContent = templateContent.Replace("**YearsAtResidence**", txtYearsAtResidence.Text);
            
            templateContent = templateContent.Replace("**Address**", txtAddress.Text);

            templateContent = templateContent.Replace("**Address2**", txtAddress2.Text);

            templateContent = templateContent.Replace("**Country**", txtCountry.Text);
            templateContent = templateContent.Replace("**Region**", txtRegion.Text);
            templateContent = templateContent.Replace("**City**", txtCity.Text);
            templateContent = templateContent.Replace("**PostalCode**", txtPostalCode.Text);

            //templateContent = templateContent.Replace("**DoYouRentOrOwn**", radRentOwn.SelectedValue);
            templateContent = templateContent.Replace("**CellPhone**", hdCellPhoneNumber.Value);
            templateContent = templateContent.Replace("**OfficePhone**", hdOfficePhone.Value);
            templateContent = templateContent.Replace("**HomePhone**", hdHomePhone.Value);
            templateContent = templateContent.Replace("**DateOfBirth**", txtDateOfBirthMonth.Text.Trim() + "/" + txtDateOfBirthDay.Text.Trim() + "/" + txtDateOfBirthYear.Text.Trim());
            templateContent = templateContent.Replace("**TaxIDNumber**", txtTaxID.Text);

            templateContent = templateContent.Replace("**Citizenship**", txtCitizenship.Text);
            templateContent = templateContent.Replace("**AlienStatus**", txtAlienStatus.Text);
            

            templateContent = templateContent.Replace("**EmployerName**", txtEmployerName.Text);
            templateContent = templateContent.Replace("**EmpAddress**", txtEmployerAddress.Text);
            templateContent = templateContent.Replace("**EmpAddress2**", txtEmpAddress2.Text);
            templateContent = templateContent.Replace("**EmpCountry**", txtEmpCountry.Text);
            templateContent = templateContent.Replace("**EmpRegion**", txtEmpRegion.Text);
            templateContent = templateContent.Replace("**EmpCity**", txtEmpCity.Text);
            templateContent = templateContent.Replace("**EmpPostalCode**", txtEmpPostalCode.Text);
            templateContent = templateContent.Replace("**EmployerPhone**", txtEmployerPhone.Text);
            



            templateContent = templateContent.Replace("**Occupation**", txtOccupation.Text);
            templateContent = templateContent.Replace("**OccupationElaborate**", txtOccupationElaborate.Text);
            templateContent = templateContent.Replace("**InvestmentExperienceYear**", txtInvestmentExperienceYear.Text);
            templateContent = templateContent.Replace("**PublicCompanies**", radPublicCompanies.SelectedValue);
            //templateContent = templateContent.Replace("**BrokerageAccounts**", radBrokerageAccounts.SelectedValue);
            //templateContent = templateContent.Replace("**BrokerageAccountsPlace**", txtBrokerageAccountsPlace.Text);
            //templateContent = templateContent.Replace("**SpouseName**", txtSpouseName.Text);
            //templateContent = templateContent.Replace("**SpouseEmployer**", txtSpouseEmployer.Text);
            //templateContent = templateContent.Replace("**SpouseOccupation**", txtSpouseOccupation.Text);
            //templateContent = templateContent.Replace("**SpouseIncome**", txtSpouseIncome.Text);
            templateContent = templateContent.Replace("**InstitutionName**", txtInstitutionName.Text);

            //templateContent = templateContent.Replace("**LiquidAssets**", txtLiquidAssets.Text);
            //templateContent = templateContent.Replace("**GrossFixedAssets**", txtGrossFixedAssets.Text);
            //templateContent = templateContent.Replace("**Liabilities**", txtLiabilities.Text);

            //templateContent = templateContent.Replace("**TotalNetWorth**", txtTotalNetWorth.Text);

            //templateContent = templateContent.Replace("**AnnualIncomeFromAllSources**", txtAnnualIncomeFromAllSources.Text);

            templateContent = templateContent.Replace("**InvestmentAdvisor**", ddlAdvisor.SelectedItem.Text);
            //txtInvestmentAdvisor.Text

            //templateContent = templateContent.Replace("**BankName**", txtBankName.Text);
            //templateContent = templateContent.Replace("**AccountNumber**", txtAccountNumber.Text);
            //templateContent = templateContent.Replace("**TransitNumber**", txtTransitNumber.Text);
            //templateContent = templateContent.Replace("**BranchAddress**", txtBranchAddress.Text);

            MailingProvider mp = new MailingProvider();
            mp.Subject = EmailSubject;
            mp.ToAddr = emailAddress;
            mp.Body = templateContent;
            mp.FromAddr = FromEmailAddress;
            mp.SendMail();

        }
        catch (Exception ex)
        {
            WriteLog("HUSA Security Form", "Email notification error: \r\n" + ex.Message + "\r\n" + ex.StackTrace);
        }
    }

    protected string GetSecurityAdvisorEmail(string advisorName)
    {
        string advisorEmail = "";


        try
        {
            // Set the provider name for the DynamicModuleManager here. All available providers are listed in
            // Administration -> Settings -> Advanced -> DynamicModules -> Providers
            var providerName = String.Empty;

            // Set a transaction name
            var transactionName = "AdvisorTransactionName";

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
            Type advisorType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.SecurityAdvisor.Securityadvisor");


            // This is how we get the collection of Advisor items
            DynamicContent advisorItem = dynamicModuleManager.GetDataItems(advisorType).Where(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live && x.Visible == true
                            && x.GetValue<string>("Title").ToString().ToLower() == advisorName.ToLower()).FirstOrDefault();

            if (advisorItem != null)
            {
                if (advisorItem.GetValue<Lstring>("Email") != null)
                    advisorEmail = advisorItem.GetValue<Lstring>("Email").ToString();
            }

        }
        catch (Exception ex)
        {
            WriteLog("HUSASecurityForm", "Get security advisor email from Sitefinity module" + "\r\n" + ex.Message + "\r\n" + ex.StackTrace);
        }

        return advisorEmail;
    }

    protected void WriteLog(string fileName, string message)
    {

        var filePath = System.AppDomain.CurrentDomain.BaseDirectory + @"App_data\log\";
        fileName = fileName + "-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
        string fullPath = filePath + fileName;
        if (File.Exists(fullPath))
        {
            //File.Delete(fullPath);
        }
        else
        {
            Directory.CreateDirectory(filePath);
        }

        FileStream filewriter = new FileStream(fullPath, FileMode.Append, FileAccess.Write);
        using (StreamWriter streamWriter = new StreamWriter(filewriter))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(message);
            streamWriter.WriteLine("{0} {1} \r\n {2}", DateTime.Now.ToLongTimeString(),
            DateTime.Now.ToLongDateString(), sb.ToString());

            streamWriter.Write("\r\n");
            streamWriter.Write("========================================================");
            streamWriter.Write("\r\n");
            streamWriter.Flush();
            streamWriter.Close();
        }
        filewriter.Close();
    }

    protected string GetSiteRoot()
    {
        string Port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
        if (Port == null || Port == "80" || Port == "443")
            Port = "";
        else
            Port = ":" + Port;

        string Protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
        if (Protocol == null || Protocol == "0")
            Protocol = "http://";
        else
            Protocol = "https://";

        string appPath = System.Web.HttpContext.Current.Request.ApplicationPath;
        if (appPath == "/")
            appPath = "";

        string sOut = Protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + Port + appPath;
        return sOut;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/husa-security-form");
    }

    protected List<string> GetSecurityAdvisors()
    {
        List<string> listCollection = new List<string>();


        try
        {
            IQueryable<DynamicContent> myCollection = null;
            // Set the provider name for the DynamicModuleManager here. All available providers are listed in
            // Administration -> Settings -> Advanced -> DynamicModules -> Providers
            var providerName = String.Empty;

            // Set a transaction name
            var transactionName = "AdvisorTransactionName";

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
            Type advisorType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.SecurityAdvisor.Securityadvisor");


            // This is how we get the collection of Advisor items
            myCollection = dynamicModuleManager.GetDataItems(advisorType).Where(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live && x.Visible == true
                            && x.GetValue<bool>("IsActive") == true).OrderBy(x => x.GetValue<string>("Title"));
            listCollection.Add("");
            listCollection.Add("Advisor Not Found");
            foreach (DynamicContent item in myCollection)
            {
                if (item.GetValue<Lstring>("Title") != null)
                    listCollection.Add(item.GetValue<Lstring>("Title").ToString());
            }

        }
        catch (Exception ex)
        {
            WriteLog("HUSASecurityForm", "Get security advisor information from Sitefinity module" + "\r\n" + ex.Message + "\r\n" + ex.StackTrace);
        }

        return listCollection;
    }
}