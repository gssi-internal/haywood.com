﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Copy of Transactions.ascx.cs" Inherits="UserControls_GSSI_Transactions"  Debug="true"%>
<asp:Repeater ID="rptlist" runat="server" EnableViewState="true" Visible="true">
    <HeaderTemplate>
   
            <div > 
                <table class="sfitem sfClearfix" >
                    <tr>
                        <th colspan="2" style="width:40%">Transactions</th>
                        
                        <th colspan="2" style="width:30%">
                            <asp:LinkButton ID="lnkBtnCategory" runat="server"  >Category</asp:LinkButton>

                        </th>
                        
                        
                    </tr>
    </HeaderTemplate>
    <ItemTemplate>
        
        
          <tr class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate-row" %>">
              
              <td colspan="2"><a ><%# Eval("Title") %></a></td>

              <td colspan="2"><%# Eval("Category") %></td>
            <%--  <img src="<%# Eval("image") %>" alt="" class="thumb wp-post-image" width="205" height="155" />
             --%>
              <asp:Image ID="MyImage" runat="server" ImageUrl='<%# Eval("img") %>' />
              
              <%--<td colspan="2"><a href='<%# Eval("ItemURL") %>'><%# Eval("Title") %></a></td>--%>
              <%--<td colspan="2" style="display:none"><a href='<%# Eval("ApplyUrl")%>' class="apply-now">Apply</a></td>--%>

          </tr>  
          
        
        
        
        
       
    </ItemTemplate>
    <FooterTemplate>
        </table>
        </div>
    </FooterTemplate>
</asp:Repeater>
