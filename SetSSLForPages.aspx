﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetSSLForPages.aspx.cs" Inherits="SetSSLForPages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList id="sslchoice" runat="server" >
            <asp:ListItem Text="All"></asp:ListItem>
            <asp:ListItem Text="Backend"></asp:ListItem>
            <asp:ListItem Text="Frontend"></asp:ListItem>
        </asp:DropDownList> 
    
    </div>
        <div></div>
        <div>

            <asp:Button Text="SSLon" runat="server" OnClick="Turnssl_On" />
            <asp:Button Text="SSLoff" runat="server" OnClick="Turnssl_Off" />
        </div>
    </form>
</body>
</html>
