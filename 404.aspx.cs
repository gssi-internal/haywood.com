﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _404 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string myURL = "";
        string qs = this.Request.ServerVariables["QUERY_STRING"];


        if (this.Request.QueryString["aspxerrorpath"] != "")
        {
            myURL = this.Request.RawUrl;

        }
        else if (qs.IndexOf("404;http") > 0)
        {
            myURL = this.Request.RawUrl;
            myURL = this.Request.ServerVariables["QUERY_STRING"];
            string myURLStrip1 = "404;http://" + this.Request.ServerVariables["HTTP_HOST"] + ":80";
            string myURLStrip2 = "404;https://" + this.Request.ServerVariables["HTTP_HOST"] + ":443";
            string myURLStrip3 = "404;http://" + this.Request.ServerVariables["HTTP_HOST"];
            string domain = "http://" + this.Request.ServerVariables["HTTP_HOST"];
            myURL = myURL.Replace(myURLStrip1, "");
            myURL = myURL.Replace(myURLStrip2, "");
            myURL = myURL.Replace(myURLStrip3, "");
        }
        else
        {
            myURL = this.Server.UrlDecode(this.Request.RawUrl);
        }

        this.Response.Status = "301 Moved Permanently";

        switch (myURL.ToLower())
        {

            case "/newsroom":
                this.Response.AddHeader("Location", "/"); break;
            case "/newsroom/in-the-news":
                this.Response.AddHeader("Location", "/"); break;
            case "/newsroom/news-archives":
                this.Response.AddHeader("Location", "/"); break;
            case "/newsroom/news-detail":
                this.Response.AddHeader("Location", "/"); break;
            default:
                this.Response.Redirect("/404-page-not-found");
                break;
        }
    }
}